<span style="color: red">This document describes a draft version of Mind Web SDK and is subject to change at any time without notice.</span>

### Introduction

Mind Web SDK is a library that provides high level object-oriented API which greatly simplifies the use of
[Mind API](/) from JavaScript code running in a browser. The library includes everything you need to embed video and 
voice communication into an existing web-application or create a new one from scratch.

To create your first application with Mind Web SDK you should be familiar with JavaScript programming language and
object-oriented programming concepts. But don't worry too much: the SDK is designed to be simple so that even a
beginner can create basic applications without a lot of coding, but at the same it is flexible enough to give an
experienced developer an ability to change every detail she want. 

### Supported Browsers

Our SDK is always compatible with latest stable versions of all major web-browsers, but since a degree of support of
web technologies on web browsers is quite different, not all features of SDK work in all browsers. Use table below to
find out how well each feature is supported across the browsers: 

| Feature                     | Google Chrome* | Mozilla Firefox | Apple Safari | Microsoft Edge | Chrome for Android | Safari for iOS |
|-----------------------------|      :---:     |      :---:      |     :---:    |      :---:     |        :---:       |      :---:     |
| Video and voice             |       yes      |       yes       |      yes     |       yes      |         yes        |       yes      |
| Camera/microphone selection |       yes      |       yes       |      yes     |       yes      |         yes        |       yes      |
| Screen/window sharing       |       yes      |       yes       |      yes**   |       yes      |      view-only     |    view-only   |
| Messaging                   |       yes      |       yes       |      yes     |       yes      |         yes        |       yes      |
| Watching recordings         |       yes      |       yes       |      yes     |       yes      |         yes        |       yes      |

\* This sections also concerns other Chromium based browsers: Opera, Yandex Browser, etc  
\** Currently only entire screen can be shared in Apple Safari

### Get application ID and token

To use Mind API you will need an application ID and a token. For development purpose we suggest you to start with our
free shared application ID `00e1db72-c57d-478c-88be-3533d43c8b34` and corresponding token:
```
W1VJggwnvg2ldDdvSYES07tpLCWLDlD5nFakVJ6QSCPiZRpAMGyAzKW07OpM1IpceZ2WT5h5Mu7Ekt7WDTQzMUoQkVTRE4NdYUFE
```
They allow creating unlimited number of conferences with up to 5 participants and up to 15 minutes duration each. These
limits should be enough for you to start developing your application. To get an application ID and a token without such
limits, please fill out a form [here](https://mind.com/form/request_api).

### Getting Started

There are three main concepts in Mind Web SDK: `conference`, `participant` and `application`. The `conference` is a
virtual space with a unique ID (like a meeting room with fixed address in the real life) where human beings can
communicate to  each other using their computers. Thereby, the `participant` is a tab of a web-browser running a
web-application, or an iOS/Android device running a mobile application, or a desktop application, or finally a SIP or
H.323 phone. In the real life there is no a suitable analogy for the `application`, but the closest one is an all
powerful meeting manager whose jobs is to arrange the `conference` and invite `participants` into it. 
 
In Mind Web SDK every `conference` is represented with an object of [Conference](Conference.html) class and every
`participant` is represented with an object of [Participant](Participant.html) class. The SDK distinguishes local and
remote participants: the local participant is a tab of a web-browser where the SDK was loaded, any other participant is
considered to be remote, i.e. there could be only one local participant and a lot of remote. There is no entity which
represents the `application` because it is intended to act through [Mind API](/) directly, but the `application` is
mentioned a lot in Mind Web SDK reference. 

There are two ways how participants to communicate: text messaging and media streaming. The former is an ordinary chat
where participant send text message to each other, whereas the latter is a bit more complicated. Any `participant` can
stream two independent [media streams](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream): one with voice
from his microphone and video from his camera, and another with an arbitrary video/audio content (e.g. it can be a live
view of a screen of his computer). The other participants can watch these media streams in
[direct](Participant.html#getMediaStream) or [mixed](Conference.html#getMediaStream) mode.   

The easiest way to start learning about the Mind Web SDK is to see an example. The following HTML is the simplest
possible web-application for participating in already created conference:

```html
<!doctype html>
<html>
    <head>
        <title>Simple Conference Application</title>
        <meta charset="utf-8">
        <style>

            html, body, #video {
                height: 100%;
                margin: 0;
                padding: 0;
            }

        </style>
        <script src="https://api.mind.com/sdk.js"></script>
        <script>
            function init() {
                MindSDK.initialize().then(function() {
                    let microphone = MindSDK.getMicrophones()[0];
                    let camera = MindSDK.getCameras()[0];
                    MindSDK.join("<CONFERENCE_URI>", "<PARTICIPANT_TOKEN>", new MindSDK.ConferenceListener()).then(function(conference) {
                        let video = document.getElementById("video");
                        video.mediaStream = conference.getMediaStream();
                        let me = conference.getMe();
                        let myStream = MindSDK.createMediaStream(microphone, camera);
                        me.setMediaStream(myStream);
                        Promise.all([ microphone.acquire(), camera.acquire() ]).catch(function(error) {
                            alert("Can't acquire camera or microphone: " + error);
                        });
                    });
                });
            }
        </script>
    </head>
    <body onload="init()">
        <video id="video" autoplay></video>
    </body>
</html>
```

The example above is missing two mandatory identifiers: `<CONFERENCE_URI>` and `<PARTICIPANT_TOKEN>`. The former has to
be replaced with an URI of an existent conference, the later with a token of an existent participant. Since Mind Web
SDK can be used on behalf of a participant only, you have to create both — a conference and a participant — using [Mind
API](/) directly and token of your [Application](Application.html).
