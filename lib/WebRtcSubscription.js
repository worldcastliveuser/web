import "webrtc-adapter";

/**
 * @implements {MediaStreamAudioSupplier}
 * @implements {MediaStreamVideoSupplier}
 *
 * @package
 */
export class WebRtcSubscription {

    constructor(context, mediaURI) {
        this.audioConsumers = new Set();
        this.videoConsumers = new Set();
        this.subscribeURI = mediaURI + "/subscribe";
        this.context = context;
        this.receiveAudio = true;
        this.receiveVideo = true;
        this.pc = null;
        this.recoveryTimeoutId = null;
        this.recoveryDelay = 0;
        this.recoveryStartTimestamp = null;
        this.recoveryDuration = 0;
        this.httpPost = null;
        this.audioTransceiver = null;
        this.videoTransceiver = null;
        this.audioTrack = null;
        this.videoTrack = null;
        this.destroyed = false;
    }

    setConstraints(receiveAudio, receiveVideo) {
        if (this.receiveAudio !== receiveAudio || this.receiveVideo !== receiveVideo) {
            this.receiveAudio = receiveAudio;
            this.receiveVideo = receiveVideo;
            if (this.audioConsumers.size > 0 && receiveAudio || this.videoConsumers.size > 0 && receiveVideo) {
                this.acquire();
            } else {
                this.release();
            }
        }
}
    destroy() {
        this.destroyed = true;
        this.release();
        this.audioConsumers.clear();
        this.videoConsumers.clear();
    }

    addAudioConsumer(consumer) {
        if (!this.destroyed) {
            if (!this.audioConsumers.has(consumer) && this.audioConsumers.add(consumer)) {
                if (this.audioConsumers.size === 1 && this.receiveAudio) {
                    this.acquire();
                } else {
                    consumer.onAudioTrack(this.audioTrack);
                }
            }
        }
    }

    removeAudioConsumer(consumer) {
        if (!this.destroyed) {
            if (this.audioConsumers.delete(consumer)) {
                consumer.onAudioTrack(null);
                if (this.audioConsumers.size === 0 && this.receiveAudio) {
                    if (this.videoConsumers.size > 0 && this.receiveVideo) {
                        this.acquire();
                    } else {
                        this.release();
                    }
                }
            }
        }
    }

    addVideoConsumer(consumer) {
        if (!this.destroyed) {
            if (!this.videoConsumers.has(consumer) && this.videoConsumers.add(consumer)) {
                if (this.videoConsumers.size === 1 && this.receiveVideo) {
                    this.acquire();
                } else {
                    consumer.onVideoTrack(this.videoTrack);
                }
            }
        }
    }

    removeVideoConsumer(consumer) {
        if (!this.destroyed) {
            if (this.videoConsumers.delete(consumer)) {
                consumer.onVideoTrack(null);
                if (this.videoConsumers.size === 0 && this.receiveVideo) {
                    if (this.audioConsumers.size > 0 && this.receiveAudio) {
                        this.acquire();
                    } else {
                        this.release();
                    }
                }
            }
        }
    }

    /**
     * @private
     */
    acquire() {
        let recover = (error) => {
            console.warn("WebRTC subscription to `" + this.subscribeURI + "` will be recovered in " + this.recoveryDelay + " seconds after a failure:", error);
            this.abort();
            if (this.recoveryStartTimestamp == null) {
                this.recoveryStartTimestamp = Date.now();
            }
            this.recoveryTimeoutId = setTimeout(() => this.acquire(), this.recoveryDelay * 1000);
            this.recoveryDelay = Math.min(this.recoveryDelay + 1, 10)
        };
        try {
            if (this.pc == null || !this.pc.addTransceiver) {
                this.abort();
                this.report = {};
                this.pc = new RTCPeerConnection();
                this.pc.onicegatheringstatechange = (event) => {
                    if (this.pc.iceGatheringState === "complete" && this.pc.localDescription) { // It would be better to use `event.target.iceGatheringState ` instead, but it isn't available in Edge
                        let requestDTO = { type: "offer", sdp: this.pc.localDescription.sdp };
                        this.context.newHttpPost(this.subscribeURI, requestDTO, (cancel) => this.httpPost = { cancel: cancel }).then((responseDTO) => {
                            let description = new RTCSessionDescription({ type: "answer", sdp: responseDTO.sdp });
                            return this.pc.setRemoteDescription(description);
                        }).catch((error) => {
                            recover(error);
                        });
                    }
                };
                this.pc.onconnectionstatechange = (event) => {
                    switch (event.target.connectionState) {
                        case "connected":
                            this.recoveryDelay = 0;
                            if (this.recoveryStartTimestamp != null) {
                                this.recoveryDuration += Date.now() - this.recoveryStartTimestamp;
                                this.recoveryStartTimestamp = null;
                            }
                            break;
                        case "failed":
                            recover(new Error("Connection failed"));
                            break;
                    }
                };
                this.pc.onnegotiationneeded = (event) => {
                    this.pc.createOffer(this.pc.addTransceiver ? {} : { offerToReceiveAudio: this.audioConsumers.size > 0 && this.receiveAudio, offerToReceiveVideo: this.videoConsumers.size > 0 && this.receiveVideo }).then((description) => {
                        return this.pc.setLocalDescription(description);
                    }).then(() => {
                        if (this.pc.iceGatheringState === "complete") {
                            this.pc.onicegatheringstatechange();
                        }
                    }).catch((error) => {
                        recover(error);
                    });
                };
                if (this.pc.addTransceiver) {
                    this.audioTransceiver = this.pc.addTransceiver("audio", { direction: "inactive" });
                    this.videoTransceiver = this.pc.addTransceiver("video", { direction: "inactive" });
                } else {
                    this.pc.ontrack = (event) => {
                        if (event.track.kind === "audio") {
                            this.fireAudioTrack(event.track);
                        } else { // event.track.kind === "video"
                            this.fireVideoTrack(event.track);
                        }
                    };
                }
            }
            if (this.pc.addTransceiver) {
                if (this.audioConsumers.size > 0 && this.receiveAudio) {
                    this.audioTransceiver.direction = "recvonly";
                    this.fireAudioTrack(this.audioTransceiver.receiver.track);
                } else {
                    this.audioTransceiver.direction = "inactive";
                    this.fireAudioTrack(null);
                }
                if (this.videoConsumers.size > 0 && this.receiveVideo) {
                    this.videoTransceiver.direction = "recvonly";
                    this.fireVideoTrack(this.videoTransceiver.receiver.track);
                } else {
                    this.videoTransceiver.direction = "inactive";
                    this.fireVideoTrack(null);
                }
            } else {
                this.pc.onnegotiationneeded();
            }
        } catch (error) {
            recover(error);
        }
    }

    /**
     * @private
     */
    release() {
        this.abort();
        this.recoveryDelay = 0;
        this.recoveryStartTimestamp = null;
        this.recoveryDuration = 0;
    }

    /**
     * @private
     */
    fireAudioTrack(audioTrack) {
        if (this.audioTrack !== audioTrack) {
            this.audioTrack = audioTrack;
            for (let consumer of this.audioConsumers) {
                consumer.onAudioTrack(audioTrack);
            }
        }
    }

    /**
     * @private
     */
    fireVideoTrack(videoTrack) {
        if (this.videoTrack !== videoTrack) {
            this.videoTrack = videoTrack;
            for (let consumer of this.videoConsumers) {
                consumer.onVideoTrack(videoTrack);
            }
        }
    }

    /**
     * @private
     */
    abort() {
        this.fireAudioTrack(null);
        this.fireVideoTrack(null);
        if (this.pc != null) {
            this.pc.close();
            this.pc = null;
            this.audioTransceiver = null;
            this.videoTransceiver = null;
        }
        if (this.httpPost != null) {
            this.httpPost.cancel();
            this.httpPost = null;
        }
        if (this.recoveryTimeoutId != null) {
            clearTimeout(this.recoveryTimeoutId);
            this.recoveryTimeoutId = null;
        }
    }

    getStats() {
        if (this.pc == null) {
            return Promise.resolve(null);
        }
        return new Promise((resolve, reject) => {
            this.pc.getStats().then((report) => {
                // for (let [key, value]  of report) { console.log(value.type, value); }
                if (this.recoveryStartTimestamp != null) {
                    this.recoveryDuration += Date.now() - this.recoveryStartTimestamp;
                    this.recoveryStartTimestamp = Date.now();
                }
                report = Array.from(report.values());
                let transport = report.filter((value) => value.type.match(/^transport$/))[0] || {};
                let candidatePair = report.filter((value) => value.type === "candidate-pair" && (transport.selectedCandidatePairId ? value.id === transport.selectedCandidatePairId : value.selected))[0] || {};
                let localCandidate = report.filter((value) => value.type === "local-candidate" && value.id === candidatePair.localCandidateId)[0] || {};
                let audioInboundRtp = report.filter((value) => value.type === "inbound-rtp" && (value.kind === "audio" || value.mediaType === "audio"))[0] || {};
                let videoInboundRtp = report.filter((value) => value.type === "inbound-rtp" && (value.kind === "video" || value.mediaType === "video"))[0] || {};
                let audioTrack = report.filter((value) => value.type === "track" && 'audioLevel' in value)[0] || {};
                let videoTrack = report.filter((value) => value.type === "track" && 'frameHeight' in value)[0] || {};
                let oldReport = this.report;
                let newReport = {
                    timestamp: Date.now(),
                    downtime: this.recoveryDuration,
                    bytesReceived: Number(candidatePair.bytesReceived),
                    bytesSent: Number(candidatePair.bytesSent),
                    audioPacketsReceived: Number(audioInboundRtp.packetsReceived),
                    audioPacketsLost: Number(audioInboundRtp.packetsLost),
                    audioSamplesReceived: Number(audioTrack.totalSamplesReceived),
                    videoPacketsReceived: Number(videoInboundRtp.packetsReceived),
                    videoPacketsLost: Number(videoInboundRtp.packetsLost),
                    videoFramesDecoded: Number(videoTrack.framesDecoded)
                };
                this.report = newReport;
                let stats = {
                    id: (this.subscribeURI.includes("secondary") ? "secondarySubscription" : "subscription") + ":" + this.subscribeURI.replace(/\/media.*/, "").replace(/.*\//, ""),
                    protocol: localCandidate.protocol || "unknown",
                    downtime: Math.round((newReport.downtime - oldReport.downtime) / (newReport.timestamp - oldReport.timestamp) * 100), // %
                    rtt: Math.round(Number(candidatePair.currentRoundTripTime) * 1000), // ms
                    jitter: Math.round((isNaN(videoInboundRtp.jitter) ? Number(audioInboundRtp.jitter) : Number(videoInboundRtp.jitter)) * 1000) , // ms
                    bitrate: {
                        incoming: Math.round((newReport.bytesReceived - oldReport.bytesReceived) * 8 / (newReport.timestamp - oldReport.timestamp)), // kbit/s
                        outgoing: Math.round((newReport.bytesSent - oldReport.bytesSent) * 8 / (newReport.timestamp - oldReport.timestamp)) // kbit/s
                    },
                };
                if (newReport.audioPacketsReceived > 0) {
                    stats.audio = {
                        sps: Math.round((newReport.audioSamplesReceived - oldReport.audioSamplesReceived) / (newReport.timestamp - oldReport.timestamp) * 1000),
                        losses: Math.round((newReport.audioPacketsLost - oldReport.audioPacketsLost) / (newReport.audioPacketsReceived + newReport.audioPacketsLost - oldReport.audioPacketsReceived - oldReport.audioPacketsLost) * 100) // %
                    }
                }
                if (newReport.videoPacketsReceived > 0) {
                    stats.video = {
                        fps: Math.round((newReport.videoFramesDecoded - oldReport.videoFramesDecoded) / (newReport.timestamp - oldReport.timestamp) * 1000),
                        width: Number(videoTrack.frameWidth),
                        height: Number(videoTrack.frameHeight),
                        losses: Math.round((newReport.videoPacketsLost - oldReport.videoPacketsLost) / (newReport.videoPacketsReceived + newReport.videoPacketsLost - oldReport.videoPacketsReceived - oldReport.videoPacketsLost) * 100) // %
                    }
                }
                resolve(stats);
            });
        });
    }

    static createRawMediaStream(stream) {
        let rawStream = new window.MediaStream();
        rawStream.onAudioTrack = (audioTrack) => {
            if (rawStream.getAudioTracks()[0] !== audioTrack) {
                if (rawStream.getAudioTracks().length > 0) {
                    rawStream.removeTrack(rawStream.getAudioTracks()[0], true);
                }
                if (audioTrack != null) {
                    rawStream.addTrack(audioTrack, true);
                }
            }
        };
        rawStream.onVideoTrack = (videoTrack) => {
            if (rawStream.getVideoTracks()[0] !== videoTrack) {
                if (rawStream.getVideoTracks().length > 0) {
                    rawStream.removeTrack(rawStream.getVideoTracks()[0], true);
                }
                if (videoTrack != null) {
                    rawStream.addTrack(videoTrack, true);
                }
            }
        };
        rawStream.close = () => {
            stream.removeAudioConsumer(rawStream);
            stream.removeVideoConsumer(rawStream);
        };
        // We override `MediaStream.addTrack` because it doesn't cause calling of `onaddtrack`
        let addTrack = rawStream.addTrack;
        rawStream.addTrack = (track, notify) => {
            addTrack.call(rawStream, track);
            if (notify && rawStream.onaddtrack) {
                rawStream.onaddtrack(new MediaStreamTrackEvent("onaddtrack", { track: track }));
            }
        };
        // We override `MediaStream.addTrack` because it doesn't cause calling of `onremovetrack`
        let removeTrack = rawStream.removeTrack;
        rawStream.removeTrack = (track, notify) => {
            removeTrack.call(rawStream, track);
            if (notify && rawStream.onremovetrack) {
                rawStream.onremovetrack(new MediaStreamTrackEvent("onremovetrack", { track: track }));
            }
        };
        return rawStream;
    }

}
