import {Conference} from "./Conference";
import {Participant} from "./Participant";
import {ParticipantRole} from "./ParticipantRole";

/**
 * ConferenceListener can be used for getting notifications of all changes in the {@link Conference} made by the server
 * part of your application and remote participants. For example, if one of the remote participants who played a role
 * of a {@link ParticipantRole.MODERATOR moderator} changed the name of the conference, then
 * {@link ConferenceListener#onConferenceNameChanged onConferenceNameChanged} method of the listener would be called.
 *
 * @interface
 */
export class ConferenceListener {

    /**
     * This method is called when the name of the conference is changed by either the server part of your application
     * or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {String} name The new name of the conference.
     */
    onConferenceNameChanged(name) {
        console.warn("Conference listener `onConferenceNameChanged` not overridden");
    }

    /**
     * This method is called when the recording of the conference is started/resumed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Conference} conference The conference the recording of which was started or resumed.
     */
    onConferenceRecordingStarted(conference) {
        console.warn("Conference listener `onConferenceRecordingStarted` method not overridden");
    }

    /**
     * This method is called when the recording of the conference is paused/stopped by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Conference} conference The conference the recording of which was paused or stopped.
     */
    onConferenceRecordingStopped(conference) {
        console.warn("Conference listener `onConferenceRecordingStopped` method not overridden");
    }

    /**
     * @deprecated Either {@link ConferenceListener#onMeReceivedMessageFromApplication} and
     *             {@link ConferenceListener#onMeReceivedMessageFromParticipant} instead.
     */
    onConferenceMessageReceived(message) {}

    /**
     * This method is called when the conference is ended by the server part of your application.
     *
     * @param {Conference} conference The conference that was ended.
     */
    onConferenceEnded(conference) {
        console.warn("Conference listener `onConferenceEnded` method not overridden");
    }

    /**
     * This method is called when a remote participant joins the conference.
     *
     * @param {Participant} participant The remote participant who joined the conference.
     */
    onParticipantJoined(participant) {
        console.warn("Conference listener `onParticipantJoined` method not overridden");
    }

    /**
     * This method is called when a remote participant leaves the conference because of his own will or because the
     * server part of your application expelled him.
     *
     * @param {Participant} participant The remote participant who left the conference.
     */
    onParticipantExited(participant) {
        console.warn("Conference listener `onParticipantExited` method not overridden");
    }

    /**
     * @deprecated Use {@link ConferenceListener#onMeExpelled} instead.
     */
    onParticipantExpelled(participant) {}

    /**
     * This method is called when the name of a remote participant is changed by the server part of your application or
     * by one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator} or by the
     * participant himself.
     *
     * @param {Participant} participant The remote participant whose name was changed.
     */
    onParticipantNameChanged(participant) {
        console.warn("Conference listener `onParticipantNameChanged` method not overridden");
    }

    /**
     * This method is called when the role of a remote participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Participant} participant The remote participant whose role was changed.
     */
    onParticipantRoleChanged(participant) {
        console.warn("Conference listener `onParticipantRoleChanged` method not overridden");
    }

    /**
     * This method is called when the layout of a remote participant is changed by the server part of your application
     * or by one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator} or by the
     * participant himself.
     *
     * @param {Participant} participant The remote participant whose layout was changed.
     */
    onParticipantLayoutChanged(participant) {
        console.warn("Conference listener `onParticipantLayoutChanged` method not overridden");
    }

    /**
     * This method is called when a remote participant starts or stops streaming his primary audio or/and video.
     *
     * @param {Participant} participant The remote participant who started or stopped streaming his primary audio
     *                                  or/and video.
     */
    onParticipantMediaChanged(participant) {
        console.warn("Conference listener `onParticipantMediaChanged` method not overridden");
    }

    /**
     * This method is called when a remote participant starts or stops streaming his secondary audio or/and video.
     *
     * @param {Participant} participant The remote participant who started or stopped streaming his secondary audio
     *                                  or/and video.
     */
    onParticipantSecondaryMediaChanged(participant) {
        console.warn("Conference listener `onParticipantSecondaryMediaChanged` method not overridden");
    }

    /**
     * This method is called when the local participant is expelled from the conference by the server part of your
     * application.
     *
     * @param {Me} me The local participant who was expelled from the conference.
     */
    onMeExpelled(me) {
        console.warn("Conference listener `onMeExpelled` method not overridden");
        this.onParticipantExpelled(me);
    }

    /**
     * This method is called when the name of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Me} me The local participant whose name was changed.
     */
    onMeNameChanged(me) {
        console.warn("Conference listener `onMeNameChanged` method not overridden");
        this.onParticipantNameChanged(me, me.getName());
    }

    /**
     * This method is called when the role of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Me} me The local participant whose role was changed.
     */
    onMeRoleChanged(me) {
        console.warn("Conference listener `onMeRoleChanged` method not overridden");
        this.onParticipantRoleChanged(me, me.getRole());
    }

    /**
     * This method is called when the layout of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {Me} me The local participant whose layout was changed.
     */
    onMeLayoutChanged(me) {
        console.warn("Conference listener `onMeLayoutChanged` method not overridden");
        this.onParticipantLayoutChanged(me, me.getLayout());
    }

    /**
     * This method is called when the local participant receives a message from the server part of your application.
     *
     * @param {Me} me The local participant whose received the message.
     * @param {String} message The text of the message.
     */
    onMeReceivedMessageFromApplication(me, message) {
        console.warn("Conference listener `onMeReceivedMessageFromApplication` method not overridden");
        this.onConferenceMessageReceived(arguments[2]);
    }

    /**
     * This method is called when the local participant receives a message from a remote participant.
     *
     * @param {Me} me The local participant whose received the message.
     * @param {String} message The text of the message.
     * @param {Participant} participant The remote participant who sent the message.
     */
    onMeReceivedMessageFromParticipant(me, message, participant) {
        console.warn("Conference listener `onMeReceivedMessageFromParticipant` method not overridden");
        this.onConferenceMessageReceived(arguments[3]);
    }

    /**
     * This method is called when the quality of the network changes. The quality is estimated as
     * {@link https://en.wikipedia.org/wiki/Mean_opinion_score mean opinion score} which is in range from 0 to 5,
     * where 0 means no network connection at all and 5 means excellent network.
     *
     * @param {Number} quality The estimated quality of the network as a number in range from 0 to 5.
     */
    onNetworkQualityChanged(quality) {
        console.warn("Conference listener `onNetworkQualityChanged` method not overridden");
    }

}