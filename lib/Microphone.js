import {MediaStream} from "./MediaStream";
import {MicrophoneListener} from "./MicrophoneListener";
import {MindSDK} from "./MindSDK";

/**
 * Microphone class is used for representing microphones plugged in to the computer (each instance represents a single
 * microphone). The list of all available microphones can be got with {@link MindSDK.getMicrophones getMicrophones}
 * method of {@link MindSDK} class. Microphone class implements {@link MediaStreamAudioSupplier} interface, so it can
 * be used as a source of audio for local {@link MediaStream}.
 *
 * ```
 * let microphone = MindSDK.getMicrophones()[0];
 * if (microphone) {
 *     let myStream = MindSDK.createMediaStream(microphone, null);
 *     me.setMediaStream(myStream);
 *     microphone.acquire().catch(function(error) {
 *         alert("Microphone can't be acquired: " + error);
 *     });
 * } else {
 *     alert("Microphone isn't plugged in");
 * }
 * ```
 *
 * @implements {MediaStreamAudioSupplier}
 */
export class Microphone {

    /**
     * @hideconstructor
     */
    constructor(id, label) {
        this.consumers = new Set();
        this.operational = true;
        this.id = id;
        this.label = label;
        this.listener = null;
        this.audioTrack = null;
    }

    /**
     * Returns the ID of the microphone. The ID is unique among all microphones and never changes.
     *
     * @returns {String} The ID of the microphone.
     */
    getId() {
        return this.id;
    }

    /**
     * @deprecated To check if the microphone is operational or not, look at the list of the
     * {@link MindSDK.getMicrophones available microphone} — if the microphone is there, then it is operational,
     * otherwise it isn't.
     */
    isOperational() {
        return this.operational;
    }

    /**
     * Returns the label of the microphone.
     *
     * @returns {String} The label of the microphone.
     */
    getLabel() {
        return this.label;
    }

    /**
     * Sets the listener which should be notified of all events related to the microphone.
     *
     * @param {MicrophoneListener} listener The listener which should be notified of all events related to the
     *                                      microphone.
     */
    setListener(listener) {
        this.listener = listener;
    }

    /**
     * Starts microphone recording. This is an asynchronous operation which assumes acquiring the underlying microphone
     * device and distributing microphone's audio among all {@link MediaStream consumers}. This method returns a
     * `Promise` that resolves with no value (if the microphone recording starts successfully) or rejects with an
     * `Error` (if there is no permission to access the microphone or if the microphone was unplugged). If the
     * microphone recording has been already started, this method returns resolved `Promise`.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    acquire() {
        if (this.audioTrack != null) {
            return Promise.resolve();
        }
        return new Promise((resolve, reject) => {
            let constraints = {
                audio: {}
            };
            if (this.id !== "default") {
                constraints.video.deviceId = this.id;
            }
            navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
                if (MindSDK.getMicrophones().length === 1) {
                    navigator.mediaDevices.ondevicechange();
                }
                this.audioTrack = stream.getAudioTracks()[0];
                this.audioTrack.onended = () => {
                    this.release();
                    if (this.listener != null) {
                        this.listener.onMicrophoneReleasedForcibly(this);
                    }
                };
                this.fireAudioTrack(this.audioTrack);
                resolve();
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /**
     * Stops microphone recording. This is a synchronous operation which assumes revoking the previously distributed
     * microphone's audio and releasing the underlying microphone device. The stopping is idempotent: the method does
     * nothing if the microphone is not acquired.
     */
    release() {
        if (this.audioTrack != null) {
            this.fireAudioTrack(null);
            this.audioTrack.stop();
            this.audioTrack = null;
        }
    }

    /**
     * @package
     */
    addAudioConsumer(consumer) {
        if (!this.consumers.has(consumer) && this.consumers.add(consumer)) {
            consumer.onAudioTrack(this.audioTrack);
        }
    }

    /**
     * @package
     */
    removeAudioConsumer(consumer) {
        if (this.consumers.delete(consumer)) {
            consumer.onAudioTrack(null);
        }
    }

    /**
     * @package
     */
    destroy() {
        this.operational = false;
        if (this.audioTrack != null) {
            this.audioTrack.onended();
        }
    }

    /**
     * @private
     */
    fireAudioTrack(audioTrack) {
        for (let consumer of this.consumers) {
            consumer.onAudioTrack(audioTrack);
        }
    }

}
