import {MediaStream} from "./MediaStream";
import {CameraListener} from "./CameraListener";
import {MindSDK} from "./MindSDK";

/**
 * Camera class is used for representing cameras plugged in to the computer (each instance represents a single camera).
 * The list of all available cameras can be got with {@link MindSDK.getCameras getCameras} method of {@link MindSDK}
 * class. Camera class implements {@link MediaStreamVideoSupplier} interface, so it can be used as a source of video
 * for local {@link MediaStream}.
 *
 * ```
 * let camera = MindSDK.getCameras()[0];
 * if (camera) {
 *     camera.setResolution(1280, 720);
 *     camera.setFps(25);
 *     let myStream = MindSDK.createMediaStream(null, camera);
 *     me.setMediaStream(myStream);
 *     camera.acquire().catch(function(error) {
 *         alert("Camera can't be acquired: " + error);
 *     });
 * } else {
 *     alert("Camera isn't plugged in");
 * }
 * ```
 *
 * @implements {MediaStreamVideoSupplier}
 */
export class Camera {

    /**
     * @hideconstructor
     */
    constructor(id, label) {
        this.consumers = new Set();
        this.operational = true;
        this.id = id;
        this.label = label;
        this.width = 640;
        this.height = 360;
        this.fps = 30;
        this.listener = null;
        this.videoTrack = null;
    }

    /**
     * Returns the ID of the camera. The ID is unique among all cameras and never changes.
     *
     * @returns {String} The ID of the camera.
     */
    getId() {
        return this.id;
    }

    /**
     * @deprecated To check if the camera is operational or not, look at the list of the
     * {@link MindSDK.getCameras available cameras} — if the camera is there, then it is operational, otherwise it
     * isn't.
     */
    isOperational() {
        return this.operational;
    }

    /**
     * Returns the label of the camera.
     *
     * @returns {String} The label of the camera.
     */
    getLabel() {
        return this.label;
    }

    /**
     * Sets resolution which the camera should use for capturing video. The resolution can be set at any moment
     * regardless whether the camera is acquired or not.
     *
     * @param {Number} width The width which the camera should use for capturing video.
     * @param {Number} height The height which the camera should use for capturing video.
     */
    setResolution(width, height) {
        if (width <= 0 || height <= 0) {
            throw new Error("Can't change resolution to `" + width + "x" + height + "`");
        }
        if (this.width !== width || this.height !== height) {
            this.width = width;
            this.height = height;
            if (this.videoTrack) {
                this.videoTrack.applyConstraints({ width: this.width, height: this.height, frameRate: this.fps });
            }
        }
    }

    /**
     * Sets frame rate which the camera should use for capturing video. The frame rate can be set at any moment
     * regardless whether the camera is acquired or not.
     *
     * @param {Number} fps The frame rate which the camera should use for capturing video.
     */
    setFps(fps) {
        if (fps <= 0) {
            throw new Error("Can't change FPS to `" + fps + "`");
        }
        if (this.fps !== fps) {
            this.fps = fps;
            if (this.videoTrack) {
                this.videoTrack.applyConstraints({ width: this.width, height: this.height, frameRate: this.fps });
            }
        }
    }

    /**
     * Sets the listener which should be notified of all events related to the camera.
     *
     * @param {CameraListener} listener The listener which should be notified of all events related to the camera.
     */
    setListener(listener) {
        this.listener = listener;
    }

    /**
     * Starts camera capturing. This is an asynchronous operation which assumes acquiring the underlying camera device
     * and distributing camera's video among all {@link MediaStream consumers}. This method returns a `Promise` that
     * resolves with no value (if the camera capturing starts successfully) or rejects with an `Error` (if there is no
     * permission to access the camera or if the camera was unplugged). If the camera capturing has been already
     * started, this method returns resolved `Promise`.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    acquire() {
        if (this.videoTrack != null) {
            return Promise.resolve();
        }
        return new Promise((resolve, reject) => {
            let constraints = {
                video: {
                    width: this.width,
                    height: this.height,
                    frameRate: this.fps
                }
            };
            if (this.id !== "default") {
                constraints.video.deviceId = this.id;
            }
            navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
                if (MindSDK.getCameras().length === 1) {
                    navigator.mediaDevices.ondevicechange();
                }
                this.videoTrack = stream.getVideoTracks()[0];
                this.videoTrack.onended = () => {
                    this.release();
                    if (this.listener != null) {
                        this.listener.onCameraReleasedForcibly(this);
                    }
                };
                this.fireVideoTrack(this.videoTrack);
                resolve();
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /**
     * Stops camera capturing. This is a synchronous operation which assumes revoking the previously distributed
     * camera's video and releasing the underlying camera device. The stopping is idempotent: the method does nothing
     * if the camera is not acquired.
     */
    release() {
        if (this.videoTrack != null) {
            this.fireVideoTrack(null);
            this.videoTrack.stop();
            this.videoTrack = null;
        }
    }

    /**
     * @package
     */
    addVideoConsumer(consumer) {
        if (!this.consumers.has(consumer) && this.consumers.add(consumer)) {
            consumer.onVideoTrack(this.videoTrack);
        }
    }

    /**
     * @package
     */
    removeVideoConsumer(consumer) {
        if (this.consumers.delete(consumer)) {
            consumer.onVideoTrack(null);
        }
    }

    /**
     * @package
     */
    destroy() {
        this.operational = false;
        if (this.videoTrack != null) {
            this.videoTrack.onended();
        }
    }

    /**
     * @private
     */
    fireVideoTrack(videoTrack) {
        for (let consumer of this.consumers) {
            consumer.onVideoTrack(videoTrack);
        }
    }

}
