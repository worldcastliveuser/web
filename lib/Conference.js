import {ConferenceContext} from "./ConferenceContext";
import {ConferenceListener} from "./ConferenceListener";
import {ConferenceWebSocket} from "./ConferenceWebSocket";
import {Me} from "./Me";
import {MediaStream} from "./MediaStream";
import {Message} from "./Message";
import {Participant} from "./Participant";
import {WebRtcSubscription} from "./WebRtcSubscription";
import mean from "lodash/mean";

/**
 * Conference class is used for representing a conferences from point of view of the {@link Me local participant}. You
 * can get a representation of the conference only after {@link MindSDK.join joining} it on behalf of one of the
 * participants. Conference class contains methods for getting and setting parameters of the conference, for
 * {@link Conference#startRecording starting} and {@link Conference#stopRecording stopping} conference recording, for
 * getting {@link Conference#getMe local} and {@link Conference#getParticipants remote} participants, and for getting
 * {@link Conference#getMediaStream conference media stream}:
 *
 * ```
 * let conferenceVideo = document.getElementById("conferenceVideo");
 * let conferenceURI = "https://api.mind.com/<APPLICATION_ID>/<CONFERENCE_ID>";
 * let participantToken = "<PARTICIPANT_TOKEN>";
 * let conferenceListener = new MindSDK.ConferenceListener();
 * MindSDK.join(conferenceURI, participantToken, conferenceListener).then(function(conference) {
 *     let conferenceStream = conference.getMediaStream();
 *     conferenceVideo.mediaStream = conferenceStream;
 * });
 * ```
 */
export class Conference {

    /**
     * @deprecated Use {@link MindSDK.join} instead.
     */
    constructor(uri, listener) {
        if (!arguments[2]) {
            console.warn("MindSDK: Direct instantiation of Conference is considered deprecated, use MindSDK.join(uri, token, listener) instead");
        }
        if (!uri || !uri.match(/^https?:\/\/[^/]+\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\/?$/)) {
            throw new Error("Conference URI is malformed");
        }
        this.uri = uri.replace(/\/+$/, "");
        this.name = null;
        this.recording = {
            started: null
        };
        this.participants = [];
        this.messages = [];
        this.index = new Map();
        this.notifications = [];
        this.context = new ConferenceContext(this.uri, listener || new ConferenceListener());
        this.subscription = new WebRtcSubscription(this.context, "/media");
        this.mediaStream = new MediaStream(this.subscription, this.subscription);
        this.rawMediaStream = WebRtcSubscription.createRawMediaStream(this.mediaStream);
        this.recoveryDelay = 0;
    }

    /**
     * @deprecated Use {@link MindSDK.join} instead.
     */
    join(token) {
        if (!arguments[1]) {
            console.warn("MindSDK: Calling Conference.join(token) is deprecated, use MindSDK.join(uri, token, listener) instead");
        }
        this.recordingURL = this.uri + "/recording?access_token=" + token;
        this.context.prepare(token);
        let joined = false;
        return new Promise((resolve, reject) => {
            let recover = (error) => {
                if (joined) {
                    console.warn("Participant session will be reopened in " + this.recoveryDelay + " seconds after a failure:", error);
                    this.notifications = [];
                    this.recoveryTimeoutId = setTimeout(() => this.ws.open(), this.recoveryDelay * 1000);
                    this.recoveryDelay = Math.min(this.recoveryDelay + 1, 10);
                } else {
                    reject(error);
                }
            };
            this.ws = new ConferenceWebSocket(this.context);
            this.ws.onOpened = () => {
                console.info("Participant session opened");
                this.context.newHttpGet("/?detailed=true").then((responseDTO) => {
                    if (!this.me) {
                        this.me = Me.fromDTO(this.context, responseDTO.participants[0]);
                        this.index.set("/participants/" + this.me.id, this.me);
                        this.updateEntireModel(responseDTO, false);
                        this.networkQuality = 5;
                        this.networkQualityAnalyzationIntervalId = setInterval(() => this.analyzeNetworkQuality(), 1000);
                    } else {
                        this.updateEntireModel(responseDTO, true);
                    }
                    this.recoveryDelay = 0;
                    if (!joined) {
                        joined = true;
                        resolve(this.me);
                        this.processNotifications();
                    }
                }).catch((error) => {
                    recover(error);
                });
            };
            this.ws.onMessageReceived = (notification) => {
                this.notifications.push(notification);
                if (joined) {
                    this.processNotifications();
                }
            };
            this.ws.onStagnated = () => {
                console.warn("Participant session stagnated");
            };
            this.ws.onRecovered = () => {
                console.warn("Participant session recovered");
            };
            this.ws.onClosed = (code) => {
                switch (code) {
                    case 4000:
                        this.notifications.push("{\"type\":\"deleted\",\"location\":\"/\"}");
                        if (joined) {
                            this.processNotifications();
                        }
                        break;
                    case 4001:
                        this.notifications.push("{\"type\":\"deleted\",\"location\":\"/participants/me\"}"); // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
                        if (joined) {
                            this.processNotifications();
                        }
                        break;
                    default:
                        recover(new Error("WebSocket closed: " + code));
                }
            };
            this.ws.open();
        });
    }

    /**
     * @deprecated Use {@link MindSDK.exit} instead.
     */
    exit() {
        if (!arguments[0]) {
            console.warn("MindSDK: Calling Conference.exit() is deprecated, use MindSDK.exit(conference) instead");
        }
        clearInterval(this.networkQualityAnalyzationIntervalId);
        if (this.recoveryTimeoutId) {
            clearTimeout(this.recoveryTimeoutId);
            this.recoveryTimeoutId = null;
        }
        if (this.me) {
            this.me.destroy();
            this.me = null;
        }
        this.participants.forEach((participant) => {
            participant.destroy();
        });
        this.subscription.destroy();
        if (this.ws) {
            this.ws.close(true);
            this.ws = null;
        }
        this.context.destroy();
    }

    /**
     * Returns the ID of the conference. The ID is unique and never changes.
     *
     * @returns {String} The ID of the conference.
     */
    getId() {
        return this.context.getConferenceId();
    }

    /**
     * Returns the current name of the conference. The name of the conference can be shown above the video in the
     * conference media stream and recording.
     *
     * @returns {String} The current name of the conference.
     */
    getName() {
        return this.name;
    }

    /**
     * Changes the name of the conference. The name of the conference can be shown above the video in the conference
     * media stream and recording. The name changing is an asynchronous operation, that's why this method returns a
     * `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the
     * operation fails). The operation can succeed only if the {@link Me local participant} plays a role of a
     * {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {String} name The new name for the conference.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setName(name) {
        let requestDTO = { name: name };
        return this.context.newHttpPatch("/", requestDTO).then((responseDTO) => {
            this.name = responseDTO.name;
        });
    }

    /**
     * Returns the {@link Me local participant}.
     *
     * @returns {Me} The local participant.
     */
    getMe() {
        return this.me;
    }

    /**
     * Returns the list of all online {@link Participant remote participants}.
     *
     * @returns {Participant[]} The list of all remote participants.
     */
    getParticipants() {
        return this.participants;
    }

    /**
     * Returns {@link Participant remote participant} with the specified ID or `null` value, if it doesn't exists or
     * if it is offline.
     *
     * @returns {Participant|null} The remote participant or `null` value, if it doesn't exists or if it is offline.
     */
    getParticipantById(id) {
        return this.index.get("/participants/" + id);
    }

    /**
     * @deprecated Persistent message are deprecated, so that this method will be removed soon.
     */
    getMessages() {
        return this.messages;
    }

    /**
     * Checks whether the conference is being recorded or not.
     *
     * @returns {Boolean} The boolean value which indicates if the conference is being recorded or not.
     */
    isRecording() {
        return this.recording.started;
    }

    /**
     * Starts recording of the conference. This is an asynchronous operation, that's why this method returns a
     * `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the
     * operation fails). The operation can succeed only if the {@link Me local participant} plays a role of a
     * {@link ParticipantRole.MODERATOR moderator}.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    startRecording() {
        let requestDTO = {};
        return this.context.newHttpPost("/recording/start", requestDTO).then((responseDTO) => {
            this.recording.started = true;
        });
    }

    /**
     * Stops recording of the conference. This is an asynchronous operation, that's why this method returns a
     * `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the
     * operation fails). The operation can succeed only if the {@link Me local participant} plays a role of a
     * {@link ParticipantRole.MODERATOR moderator}.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    stopRecording() {
        let requestDTO = {};
        return this.context.newHttpPost("/recording/stop", requestDTO).then((responseDTO) => {
            this.recording.started = false;
        });
    }

    /**
     * Returns a URL for downloading the recording of the conference. The returned URL can be used for downloading only
     * if the {@link Me local participant} plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @returns {String} The URL for downloading the recording of the conference.
     */
    getRecordingURL() {
        return this.recordingURL;
    }

    /**
     * Returns {@link MediaStream media stream} of the conference. The returned media stream stream is a mix of all
     * audios and videos (excluding only audio of the local participant) that participants are streaming at the moment.
     * The videos in the media stream are arranged using {@link Me#getLayout current layout} of the
     * {@link Me local participant}. You can get and play the media stream of the conference at any time.
     *
     * @returns {MediaStream} The media steam of the conference.
     */
    getMediaStream() {
        if (arguments.length > 0) {
            console.warn("MindSDK: Calling Conference.getMediaStream(mode) is deprecated, use Conference.getMediaStream() instead");
            if (arguments[0].audio) {
                this.mediaStream.addAudioConsumer(this.rawMediaStream);
            } else {
                this.mediaStream.removeAudioConsumer(this.rawMediaStream);
            }
            if (arguments[0].video) {
                this.mediaStream.addVideoConsumer(this.rawMediaStream);
            } else {
                this.mediaStream.removeVideoConsumer(this.rawMediaStream);
            }
            return this.rawMediaStream;
        } else {
            return this.mediaStream;
        }
    }

    /**
     * @deprecated This method is deprecated and will be removed soon. Consider using {@link Me#sendMessageToAll} and
     * {@link Me#sendMessageToParticipant} instead.
     */
    sendMessage(message, participant) {
        let requestDTO = { sendTo: participant || null, text: message, persistent: true };
        return this.context.newHttpPost("/messages", requestDTO).then((responseDTO) => {
            this.createModelItem("/messages/" + responseDTO.id, responseDTO, false);
        });
    }

    /**
     * Returns current network quality as [mean opinion score]{@link https://en.wikipedia.org/wiki/Mean_opinion_score}.
     * in range from 0 to 5, where 0 means no network connection at all and 5 means excellent network.
     *
     * @returns {Number} The estimated quality of the network as a number in range from 0 to 5.
     */
    getNetworkQuality() {
        return this.networkQuality;
    }

    /**
     * @private
     */
    updateEntireModel(dto, fire) {
        this.updateModelItem("/", dto, fire);
        let participantDTOs = dto.participants;
        // Build a set of IDs of online participants
        let onlineParticipantIDs = new Set();
        participantDTOs.forEach((participantDTO) => {
            if (participantDTO.online) {
                onlineParticipantIDs.add(participantDTO.id);
            }
        });
        // Remove missing and offline participants
        for (let i = this.participants.length - 1; i >= 0 ; i--) {
            if (!onlineParticipantIDs.has(this.participants[i].id)) {
                this.deleteModelItem("/participants/" + this.participants[i].id, fire);
            }
        }
        // Update existent participants and add new participants
        participantDTOs.forEach((participantDTO) => {
            if (participantDTO.online) {
                if (this.index.has("/participants/" + participantDTO.id)) {
                    this.updateModelItem("/participants/" + participantDTO.id, participantDTO, fire);
                } else {
                    this.createModelItem("/participants/" + participantDTO.id, participantDTO, fire);
                }
            }
        });
        // Add new messages
        dto.messages.forEach((messageDTO) => this.createModelItem("/messages/" + messageDTO.id, messageDTO, fire));
    }

    /**
     * @private
     */
    createModelItem(location, dto, fire) {
        if (location.startsWith("/participants/")) {
            if (dto.online) {
                let participant = Participant.fromDTO(this.context, dto);
                this.participants.push(participant);
                this.index.set(location, participant);
                if (fire) {
                    this.context.fireOnParticipantJoined(participant);
                }
            }
        } else if (location.startsWith("/messages/")) {
            let message = Message.fromDTO(this, dto);
            if (message.persistent) {
                this.messages.push(message);
                this.index.set(location, message); // FIXME: Do we really need to index messages, since they are never deleted?
            }
            if (fire) {
                if (message.getSentBy() === this.context.getApplicationId()) {
                    this.context.fireOnMeReceivedMessageFromApplication(this.me, message);
                } else {
                    this.context.fireOnMeReceivedMessageFromParticipant(this.me, message, this.getParticipantById(message.getSentBy()));
                }
            }
        }
    }

    /**
     * @private
     */
    updateModelItem(location, dto, fire) {
        if (location === "/") {
            this.update(dto, fire);
        } else if (location.startsWith("/participants/")) {
            if (dto.online) {
                if (this.index.has(location)) {
                    let participant = this.index.get(location);
                    participant.update(dto, fire);
                } else {
                    this.createModelItem(location, dto, fire);
                }
            } else {
                this.deleteModelItem(location, fire);
            }
        }
    }

    /**
     * @private
     */
    deleteModelItem(location, fire) {
        if (location === "/") {
            this.context.fireOnConferenceEnded();
            this.exit();
        } else if (location === "/participants/me") {
            this.context.fireOnMeExpelled(this.me);
            this.exit();
        } else if (location.startsWith("/participants/") && this.index.has(location)) {
            let participant = this.index.get(location);
            let participantIndex = this.participants.indexOf(participant);
            if (participantIndex >= 0) {
                this.participants.splice(participantIndex, 1);
                this.index.delete(location);
                if (fire) {
                    this.context.fireOnParticipantExited(participant);
                }
            }
        }
    }

    /**
     * @private
     */
    processNotifications() {
        for (let notification of this.notifications) {
            try {
                let json = JSON.parse(notification);
                switch (json.type) {
                    case "created":
                        this.createModelItem(json.location, json.resource, true);
                        break;
                    case "updated":
                        this.updateModelItem(json.location, json.resource, true);
                        break;
                    case "deleted":
                        this.deleteModelItem(json.location, true);
                        break;
                }
            } catch (error) {
                console.warn("Can't parse notification `" + notification + "`:", error);
            }
        }
        this.notifications = [];
    }

    /**
     * @private
     */
    update(dto, fire) {
        if (this.name !== dto.name) {
            this.name = dto.name;
            if (fire) {
                this.context.fireOnConferenceNameChanged(this);
            }
        }
        if (this.recording.started !== dto.recording.started) {
            this.recording.started = dto.recording.started;
            if (fire) {
                if (this.recording.started) {
                    this.context.fireOnConferenceRecordingStarted();
                } else {
                    this.context.fireOnConferenceRecordingStopped();
                }
            }
        }
    }

    /**
     * @private
     */
    analyzeNetworkQuality() {
        let promises = [ this.subscription.getStats(), this.me.publication.getStats(), this.me.secondaryPublication.getStats() ];
        this.participants.forEach((participant) => {
            promises.push(participant.subscription.getStats());
            promises.push(participant.secondarySubscription.getStats());
        });
        Promise.all(promises).then((stats) => {
            let networkQuality = 5;
            let rtts = [];
            stats = stats.filter(report => report != null);
            for (let report of stats) {
                if (report.rtt) {
                    rtts.push(report.rtt);
                }
                if (report.id === "publication:" + this.me.id) {
                    if (report.video) {
                        if (report.video.width >= 640 && report.video.height >= 480) {
                            if (report.bitrate.outgoing < 1200) {
                                networkQuality = Math.min(networkQuality, 4);
                            }
                            if (report.bitrate.outgoing < 800) {
                                networkQuality = Math.min(networkQuality, 3);
                            }
                            if (report.bitrate.outgoing < 500) {
                                networkQuality = Math.min(networkQuality, 2);
                            }
                            if (report.bitrate.outgoing < 300) {
                                networkQuality = Math.min(networkQuality, 1);
                            }
                        }
                    }
                }
                if (report.id === "subscription:" + this.context.getConferenceId()) {
                    if (report.video) {
                        if (report.video.fps < 20) {
                            networkQuality = Math.min(networkQuality, 3);
                        }
                        if (report.video.fps < 10) {
                            networkQuality = Math.min(networkQuality, 2);
                        }
                        if (report.video.fps < 5) {
                            networkQuality = Math.min(networkQuality, 1);
                        }
                    }
                }
                if (this.ws.stagnated) {
                    networkQuality = Math.min(networkQuality, 1);
                }
                if (navigator.onLine === false || this.recoveryDelay > 0 || report.downtime > 0) {
                    networkQuality = Math.min(networkQuality, 0);
                }
            }
            let rtt = mean(rtts);
            if (rtt > 100) {
                networkQuality = Math.min(networkQuality, 4);
            }
            if (rtt > 200) {
                networkQuality = Math.min(networkQuality, 3);
            }
            if (rtt > 300) {
                networkQuality = Math.min(networkQuality, 2);
            }
            if (rtt > 500) {
                networkQuality = Math.min(networkQuality, 1);
            }
            if (this.networkQuality !== networkQuality) {
                this.networkQuality = networkQuality;
                this.context.fireOnNetworkQualityChanged(networkQuality, stats);
            }
        });
    }

}
