/**
 * @interface MediaStreamAudioConsumer
 *
 * @package
 */

/**
 * @method
 * @name MediaStreamAudioConsumer#onAudioTrack
 * @param {AudioTrack} audioTrack
 */