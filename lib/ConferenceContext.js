import axios from "axios";

export class ConferenceContext {

    constructor(conferenceURI, listener) {
        this.conferenceURI = conferenceURI;
        this.listener = listener;
    }

    getApplicationId() {
        return this.conferenceURI.split("/")[3];
    }

    getConferenceId() {
        return this.conferenceURI.split("/")[4];
    }

    prepare(token) {
        this.token = token;
        this.axios = axios.create({ timeout: 10000, headers: { "Authorization": "Bearer " + token }});
    }

    destroy() {}

    newHttpGet(relativeURI, cancellation) {
        return new Promise((resolve, reject) => {
            let config = cancellation ? { cancelToken: new axios.CancelToken(cancellation) } : {};
            this.axios.get(this.conferenceURI + relativeURI, config).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                if (!axios.isCancel(error)) {
                    reject(error);
                }
            });
        });
    }

    newHttpPost(relativeURI, dto, cancellation) {
        return new Promise((resolve, reject) => {
            let config = cancellation ? { cancelToken: new axios.CancelToken(cancellation) } : {};
            this.axios.post(this.conferenceURI + relativeURI, dto, config).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                if (!axios.isCancel(error)) {
                    reject(error);
                }
            });
        });
    }

    newHttpPatch(relativeURI, dto, cancellation) {
        return new Promise((resolve, reject) => {
            let config = cancellation ? { cancelToken: new axios.CancelToken(cancellation) } : {};
            this.axios.patch(this.conferenceURI + relativeURI, dto, config).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                if (!axios.isCancel(error)) {
                    reject(error);
                }
            });
        });
    }

    newWebSocket() {
        return new WebSocket(this.conferenceURI.replace(/^http/, "ws") + "?access_token=" + this.token);
    }

    fireOnConferenceNameChanged(conference) {
        let name = conference.getName();
        this.listener.onConferenceNameChanged(name);
    }

    fireOnConferenceRecordingStarted(conference) {
        this.listener.onConferenceRecordingStarted();
    }

    fireOnConferenceRecordingStopped(conference) {
        this.listener.onConferenceRecordingStopped();
    }

    fireOnConferenceEnded(conference) {
        this.listener.onConferenceEnded();
    }

    fireOnParticipantJoined(participant) {
        this.listener.onParticipantJoined(participant);
    }

    fireOnParticipantExited(participant) {
        this.listener.onParticipantExited(participant);
    }

    fireOnParticipantNameChanged(participant) {
        this.listener.onParticipantNameChanged(participant, participant.getName());
    }

    fireOnParticipantRoleChanged(participant) {
        this.listener.onParticipantRoleChanged(participant, participant.getRole());
    }

    fireOnParticipantLayoutChanged(participant) {
        this.listener.onParticipantLayoutChanged(participant, participant.getLayout());
    }

    fireOnParticipantMediaChanged(participant) {
        this.listener.onParticipantMediaChanged(participant, participant.isStreamingVideo(), participant.isStreamingAudio());
    }

    fireOnParticipantSecondaryMediaChanged(participant) {
        this.listener.onParticipantSecondaryMediaChanged(participant, participant.isStreamingSecondaryVideo(), participant.isStreamingSecondaryAudio());
    }

    fireOnMeExpelled(me) {
        this.listener.onMeExpelled(me);
    }

    fireOnMeNameChanged(me) {
        this.listener.onMeNameChanged(me);
    }

    fireOnMeRoleChanged(me) {
        this.listener.onMeRoleChanged(me);
    }

    fireOnMeLayoutChanged(me) {
        this.listener.onMeLayoutChanged(me);
    }

    fireOnMeReceivedMessageFromApplication(me, message) {
        this.listener.onMeReceivedMessageFromApplication(me, message.getText(), message);
    }

    fireOnMeReceivedMessageFromParticipant(me, message, participant) {
        this.listener.onMeReceivedMessageFromParticipant(me, message.getText(), participant, message);
    }

    fireOnNetworkQualityChanged(quality, stats) {
        this.listener.onNetworkQualityChanged(quality, stats);
    }

}
