import {Camera} from "./Camera";
import {Conference} from "./Conference";
import {ConferenceListener} from "./ConferenceListener";
import {MediaStream} from "./MediaStream";
import {Microphone} from "./Microphone";
import {Screen} from "./Screen";
import "webrtc-adapter";

/**
 * MindSDK class is the entry point of Mind Web SDK. It contains static methods for {@link MindSDK.join joining} and
 * {@link MindSDK.exit leaving} conferences, for getting available {@link MindSDK.getCameras cameras} and
 * {@link MindSDK.getMicrophones microphones}, and for {@link MindSDK.createMediaStream creating local media streams}.
 * But before you can do all this, the SDK should be initialized:
 *
 * ```
 * MindSDK.initialize().then(function() {
 *    // Initialization of Mind SDK is completed, now you can participate in conferences.
 * });
 * ```
 */
export class MindSDK {

    /**
     * Initializes Mind Web SDK. The initialization is an asynchronous operation, that's why this method returns a
     * `Promise` that either resolves with no value when the initialization is completed. The initialization should be
     * completed only once before calling any other method of the MindSDK class.
     *
     * @returns {Promise} The promise that resolves with no value when the initialization is completed.
     */
    static initialize() {
        if (!MindSDK.microphones) {
            MindSDK.microphones = [];
        }
        if (!MindSDK.cameras) {
            MindSDK.cameras = [];
        }
        if (!MindSDK.screen) {
            MindSDK.screen = typeof navigator.mediaDevices.getDisplayMedia === "function" ? new Screen() : null;
        }
        navigator.mediaDevices.ondevicechange = () => {
            return navigator.mediaDevices.enumerateDevices().then((devices) => {
                // Remove disconnected microphones
                for (let i = MindSDK.microphones.length - 1; i >= 0; i--) {
                    let microphone = MindSDK.microphones[i];
                    if (!devices.find((device) => device.kind === "audioinput" && (i === 0 || device.deviceId === microphone.getId()))) {
                        MindSDK.microphones.splice(i, 1)[0].destroy();
                    }
                }
                // Remove disconnected cameras
                for (let i = MindSDK.cameras.length - 1; i >= 0; i--) {
                    let camera = MindSDK.cameras[i];
                    if (!devices.find((device) => device.kind === "videoinput" && (i === 0 || device.deviceId === camera.getId()))) {
                        MindSDK.cameras.splice(i, 1)[0].destroy();
                    }
                }
                // Add newly connected microphones and cameras
                devices.forEach((device) => {
                    switch (device.kind) {
                        case "audioinput":
                            if (MindSDK.microphones.length === 0) {
                                MindSDK.microphones.push(new Microphone("default", "Default Microphone"));
                            }
                            if (device.label !== "" && device.label !== "default") {
                                let microphone = MindSDK.microphones.find((microphone) => microphone.getId() === device.deviceId);
                                if (!microphone) {
                                    MindSDK.microphones.push(new Microphone(device.deviceId, device.label));
                                }
                            }
                            break;
                        case "videoinput":
                            if (MindSDK.cameras.length === 0) {
                                if (devices.find((device) => device.kind === "videoinput")) {
                                    MindSDK.cameras.push(new Camera("default", "Default Camera"));
                                }
                            }
                            if (device.label !== "" && device.label !== "default") {
                                let camera = MindSDK.cameras.find((camera) => camera.getId() === device.deviceId);
                                if (!camera) {
                                    MindSDK.cameras.push(new Camera(device.deviceId, device.label));
                                }
                            }
                            break;
                    }
                });
            });
        }
        return navigator.mediaDevices.ondevicechange();
    }

    /**
     * Returns the list of microphones which are currently plugged in to the computer. If no microphones are plugged in
     * to the computer, the list will empty. If at least one microphone is plugged in to the computer, the contents of
     * the list will depend on whether there is permission to access microphones or not: if the permission has been
     * granted, the list will contain a default microphone and all other microphones, otherwise the list will contain
     * the default microphone only. The initial list of microphones is discovered during the
     * {@link MindSDK.initialize initialization}, but since any microphone might be plugged in and unplugged at any
     * moment, Mind Web SDK watches for changes and updates the list accordingly.
     *
     * @returns {Microphone[]} The list of microphones which are currently plugged in to the computer.
     */
    static getMicrophones() {
        return MindSDK.microphones;
    }

    /**
     * Returns the list of cameras which are currently plugged in to the computer. If no cameras are plugged in to the
     * computer, the list will empty. If at least one camera is plugged in to the computer, the contents of the list
     * will depend on whether there is permission to access cameras or not: if the permission has been granted, the
     * list will contain a default camera and all other cameras, otherwise the list will contain the default camera
     * only. The initial list of cameras is discovered during the {@link MindSDK.initialize initialization}, but since
     * any camera might be plugged in and unplugged at any moment, Mind Web SDK watches for changes and updates the
     * list accordingly.
     *
     * @returns {Camera[]} The list of cameras which are currently plugged in to the computer.
     */
    static getCameras() {
        return MindSDK.cameras;
    }

    /**
     * Returns the screen that can be used for capturing the contents of the entire screen or portion thereof (such
     * as a window of an application or a tab of the browser) or `null` value if the browser does not support screen
     * capturing.
     *
     * @returns {Screen|null} The screen or `null` value if the browser doesn't support screen capturing.
     */
    static getScreen() {
        return MindSDK.screen;
    }

    /**
     * Creates {@link MediaStream local media stream} with audio and video from the specified suppliers. The `null`
     * value can be passed instead of one of the suppliers to create audio-only or video-only media stream. Even if
     * audio/video supplier wasn't `null` it doesn't mean that the result media stream would automatically contain
     * audio/video, e.g. {@link Microphone} (as a supplier of audio) and {@link Camera} (as a supplier of video) supply
     * no audio and no video, respectively, till they are acquired and after they were released.
     *
     * @param {MediaStreamAudioSupplier|null} audioSupplier The audio supplier or `null` to create video-only media
     *                                        stream.
     * @param {MediaStreamVideoSupplier|null} videoSupplier The video supplier or `null` to create audio-only media
     *                                        stream.
     *
     * @returns {MediaStream} The created media stream.
     */
    static createMediaStream(audioSupplier, videoSupplier) {
        if (!audioSupplier && !videoSupplier) {
            throw new Error("Can't create MediaStream of `" + audioSupplier + "` audio supplier and `" + videoSupplier + "` video supplier");
        }
        return new MediaStream(audioSupplier, videoSupplier);
    }

    /**
     * Joins the conference on behalf of the participant with the specified token. The third argument is a listener
     * which will be notified about all the changes in the conference. The joining is an asynchronous operation, that's
     * why this method returns a `Promise` that either resolves with a {@link Conference} (if the operation succeeded)
     * or rejects with an `Error` (if the operation failed).
     *
     * @param {String} uri The URI of the conference.
     * @param {String} token The token of the participant on behalf of whom we are joining the conference.
     * @param {ConferenceListener} listener The listener that should notifications about all the changes in the
     *                                      conference.
     *
     * @returns {Promise<Conference>} The promise that either resolves with a {@link Conference} or rejects with an
     *                                `Error`.
     */
    static join(uri, token, listener) {
        let conference = new Conference(uri, listener, "MindSDK");
        return conference.join(token, "MindSDK").then(() => conference);
    }

    /**
     * Leaves the conference if it has been {@link MindSDK.join joined}. The leaving is an idempotent synchronous
     * operation. After the leaving the conference object itself and all other objects related to the conference aren't
     * functional.
     *
     * @param {Conference} conference The conference which we are leaving.
     */
    static exit(conference) {
        conference.exit("MindSDK");
    }

}
