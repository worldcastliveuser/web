export class ParticipantMedia {

    static fromDTO(dto) {
        return new ParticipantMedia(dto.audio, dto.video);
    }

    constructor(audio, video) {
        this.audio = audio;
        this.video = video;
    }

    isAudio() {
        return this.audio;
    }

    isVideo() {
        return this.video;
    }

}