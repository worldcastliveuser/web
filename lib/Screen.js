import {MediaStream} from "./MediaStream";
import {ScreenListener} from "./ScreenListener";

/**
 * Screen class is used for representing the display of the computer. It can be used to capture the contents of the
 * entire screen or a portion thereof (such as a window of an application or a tab of the browser). Screen class
 * implements {@link MediaStreamVideoSupplier} interface, so it can be used as a source of video for local
 * {@link MediaStream}.
 *
 * ```
 * let screen = MindSDK.getScreen();
 * if (screen) {
 *     screen.setFps(5);
 *     let mySecondaryStream = MindSDK.createMediaStream(null, screen);
 *     me.setSecondaryMediaStream(mySecondaryStream);
 *     screen.acquire().catch(function(error) {
 *         alert("Screen can't be acquired: " + error);
 *     });
 * } else {
 *     alert("Screen capturing isn't supported");
 * }
 * ```
 *
 * @implements {MediaStreamVideoSupplier}
 */
export class Screen {

    /**
     * @hideconstructor
     */
    constructor() {
        this.consumers = new Set();
        this.fps = 30;
        this.listener = null;
        this.videoTrack = null;
    }

    /**
     * Sets frame rate which the screen should use for capturing video. The frame rate can be set at any moment
     * regardless whether the screen is acquired or not.
     *
     * @param {Number} fps The frame rate which the screen should capture video at.
     */
    setFps(fps) {
        if (fps <= 0) {
            throw new Error("Can't change FPS to `" + fps + "`");
        }
        if (this.fps !== fps) {
            this.fps = fps;
            if (this.videoTrack) {
                this.videoTrack.applyConstraints({ frameRate: this.fps });
            }
        }
    }

    /**
     * Sets the listener which should be notified of all events related to the screen.
     *
     * @param {ScreenListener} listener The listener which should be notified of all events related to the screen.
     */
    setListener(listener) {
        this.listener = listener;
    }

    /**
     * Starts screen capturing. This is an asynchronous operation which assumes prompting the user to select and grant
     * permission to capture the contents of a screen or portion thereof (such as a window) and distributing screen's
     * video among all {@link MediaStream consumers}. This method returns a `Promise` that resolves with no value (if
     * the screen capturing starts successfully) or rejects with an `Error` (if the user didn't grant permission to
     * access the screen or canceled the operation). If screen capturing has been already started, this method returns
     * already resolved `Promise`.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    acquire() {
        if (this.videoTrack != null) {
            return Promise.resolve();
        }
        return new Promise((resolve, reject) => {
            navigator.mediaDevices.getDisplayMedia({ video: { frameRate: this.fps } }).then((stream) => {
                this.videoTrack = stream.getVideoTracks()[0];
                this.videoTrack.onended = () => {
                    this.release();
                    if (this.listener != null) {
                        this.listener.onScreenReleasedForcibly();
                    }
                };
                this.fireVideoTrack(this.videoTrack);
                resolve();
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /**
     * Stops screen capturing. This is a synchronous operation which assumes revoking previously distributed screen's
     * video. The stopping is idempotent: the method does nothing if the screen is not acquired.
     */
    release() {
        if (this.videoTrack != null) {
            this.fireVideoTrack(null);
            this.videoTrack.stop();
            this.videoTrack = null;
        }
    }

    /**
     * @package
     */
    addVideoConsumer(consumer) {
        if (!this.consumers.has(consumer) && this.consumers.add(consumer)) {
            consumer.onVideoTrack(this.videoTrack);
        }
    }

    /**
     * @package
     */
    removeVideoConsumer(consumer) {
        if (this.consumers.delete(consumer)) {
            consumer.onVideoTrack(null);
        }
    }

    /**
     * @private
     */
    fireVideoTrack(videoTrack) {
        for (let consumer of this.consumers) {
            consumer.onVideoTrack(videoTrack);
        }
    }

}
