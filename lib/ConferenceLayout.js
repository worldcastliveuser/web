/**
 * ConferenceLayout enumerates all available layouts for arranging videos in
 * {@link Conference#getMediaStream conference media stream}. The layout of conference media stream for the local
 * participant can be changed with {@link Me#setLayout setLayout} method of {@link Me} class. If the local participant
 * is a {@link ParticipantRole.MODERATOR moderator} than you also can change the layout of conference media stream for
 * any remote participants with {@link Participant#setLayout setLayout} method of {@link Participant} class.
 *
 * ```
 * let conferenceVideo = document.getElementById("conferenceVideo");
 * conferenceVideo.mediaStream = conference.getMediaStream();
 * // Make the `conferenceVideo` play the conference media stream in SELECTOR layout
 * me.setLayout(MindSDK.ConferenceLayout.SELECTOR)
 * ```
 *
 * @readonly
 * @enum {ConferenceLayout}
 */
const ConferenceLayout = {

    /**
     * The `MOSAIC` layout assumes displaying videos of all participants at the same time: the area is divided into
     * equal rectangular areas, each of which is given for video of one of the participants.
     */
    MOSAIC: "mosaic",

    /**
     * The `SELECTOR` layout assumes displaying video of only one participant at any moment—the one who currently is
     * the loudest.
     */
    SELECTOR: "selector",

};

export { ConferenceLayout };