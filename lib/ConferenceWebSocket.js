export class ConferenceWebSocket {

    constructor(context) {
        this.context = context;
        this.stagnated = false;
    }

    open() {
        this.close(false);
        this.ws = this.context.newWebSocket();
        this.ws.onopen = () => {
            this.ws.onmessage({ data: "pong" });
            this.onOpened();
        };
        this.ws.onmessage = (event) => {
            if (this.stagnated) {
                this.stagnated = false;
                this.onRecovered();
            }
            if (this.pingTimeoutId) {
                clearTimeout(this.pingTimeoutId);
            }
            this.pingTimeoutId = setTimeout(() => {
                this.ws.send("ping");
                this.pingTimeoutId = setTimeout(() => {
                    this.stagnated = true;
                    this.onStagnated();
                }, 5000);
            }, 5000);
            if (event.data !== "pong") {
                this.onMessageReceived(event.data);
            }
        };
        this.ws.onclose = (event) => {
            this.close(false);
            this.onClosed(event.code);
        };
    }

    close(close) {
        if (this.ws) {
            if (this.pingTimeoutId) {
                clearTimeout(this.pingTimeoutId);
                this.pingTimeoutId = null;
            }
            this.ws.onopen = null;
            this.ws.onmessage = null;
            this.ws.onclose = null;
            if (close) {
                this.ws.close();
            }
            this.ws = null;
        }
    }

    onOpened() {}

    onMessageReceived(message) {}

    onStagnated() {}

    onRecovered() {}

    onClosed(code) {}

}
