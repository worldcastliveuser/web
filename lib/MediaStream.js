/**
 * MediaStream class is used for representing audio/video content which participants can send and receive. Unlike the
 * standard {@link https://developer.mozilla.org/en-US/docs/Web/API/MediaStream MediaStream} it can contain one audio
 * and one video only. There are two types of media streams: local and remote. The distinction is nominal — still there
 * is only one class to represent both of them.
 *
 * Local media streams are created explicitly with a help of {@link MindSDK.createMediaStream createMediaStream} method
 * of {@link MindSDK} class. They can contains audio/video from local suppliers only (such as {@link Microphone},
 * {@link Camera} and {@link Screen}). Local media streams are intended to be
 * {@link Me#setMediaStream streamed on behalf of the local participant} only.
 *
 * Remote media streams contain audio/video of remote participants or the conference. The primary and the secondary
 * media streams of any remote participant can be got with help of {@link Participant#getMediaStream getMediaStream} or
 * {@link Participant#getSecondaryMediaStream getSecondaryMediaStream} methods of {@link Participant} class,
 * respectively. The media stream of the conference can be got with {@link Participant#getMediaStream getMediaStream}
 * method of {@link Conference} class.
 *
 * Any media stream can be played with `<video/>` or `<audio/>` HTML elements — just assign the stream to `mediaStream`
 * property which Mind Web SDK adds to prototypes of HTMLVideoElement and HTMLAudioElement automatically). Keep in mind
 * that `<video/>` tries to play (tries to consume) audio and video of the media stream, whereas `<audio/>` tries to
 * play (tries to consumes) audio only of the media stream. This means that if you want to play only audio of a remote
 * media stream, it is better to play it with `<audio/>` instead of playing it with invisible `<video/>` because the
 * latter will force underlying WebRTC connection to receive and decode both audio and video.
 *
 * ```
 * let myVideo = document.getElementById("myVideo");
 * let mySecondaryStream = document.getElementById("mySecondaryVideo");
 * let participantVideo = document.getElementById("participantVideo");
 *
 * let me = conference.getMe();
 *
 * // We assume that microphone and camera have been already acquired or will be acquired later
 * let myStream = MindSDK.createMediaStream(microphone, camera);
 * me.setMediaStream(myStream);
 * myVideo.mediaStream = myStream;
 *
 * // We assume that screen has been already acquired or will be acquired later
 * let mySecondaryStream = MindSDK.createMediaStream(null, screen);
 * me.setSecondaryMediaStream(mySecondaryStream);
 * mySecondaryVideo.mediaStream = mySecondaryStream;
 *
 * let participant = conference.getParticipantById("<PARTICIPANT_ID>");
 * if (participant) {
 *     participantVideo.mediaStream = participant.getMediaStream();
 * }
 *
 * ...
 *
 * let conferenceAudio = document.createElement("audio");
 * conferenceAudio.mediaStream = conference.getMediaStream();
 * document.body.append(conferenceAudio);
 * ```
 */
export class MediaStream {

    /**
     * @hideconstructor
     */
    constructor(audioSupplier, videoSupplier) {
        this.audioConsumers = new Set();
        this.videoConsumers = new Set();
        this.audioTrack = null;
        this.videoTrack = null;
        this.audioSupplier = audioSupplier;
        this.videoSupplier = videoSupplier;
    }

    /**
     * @package
     */
    onAudioTrack(audioTrack) {
        if (this.audioTrack !== audioTrack) {
            this.audioTrack = audioTrack;
            for (let consumer of this.audioConsumers) {
                consumer.onAudioTrack(audioTrack);
            }
        }
    }

    /**
     * @package
     */
    onVideoTrack(videoTrack) {
        if (this.videoTrack !== videoTrack) {
            this.videoTrack = videoTrack;
            for (let consumer of this.videoConsumers) {
                consumer.onVideoTrack(videoTrack);
            }
        }
    }

    /**
     * @package
     */
    addAudioConsumer(consumer) {
        if (!this.audioConsumers.has(consumer) && this.audioConsumers.add(consumer)) {
            if (this.audioConsumers.size === 1 && this.audioSupplier != null) {
                this.audioSupplier.addAudioConsumer(this);
            } else {
                consumer.onAudioTrack(this.audioTrack);
            }
        }
    }

    /**
     * @package
     */
    removeAudioConsumer(consumer) {
        if (this.audioConsumers.delete(consumer)) {
            consumer.onAudioTrack(null);
            if (this.audioConsumers.size === 0 && this.audioSupplier != null) {
                this.audioSupplier.removeAudioConsumer(this);
            }
        }
    }

    /**
     * @package
     */
    addVideoConsumer(consumer) {
        if (!this.videoConsumers.has(consumer) && this.videoConsumers.add(consumer)) {
            if (this.videoConsumers.size === 1 && this.videoSupplier != null) {
                this.videoSupplier.addVideoConsumer(this);
            } else {
                consumer.onVideoTrack(this.videoTrack);
            }
        }
    }

    /**
     * @package
     */
    removeVideoConsumer(consumer) {
        if (this.videoConsumers.delete(consumer)) {
            consumer.onVideoTrack(null);
            if (this.videoConsumers.size === 0 && this.videoSupplier != null) {
                this.videoSupplier.removeVideoConsumer(this);
            }
        }
    }

}
