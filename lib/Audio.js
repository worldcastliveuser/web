let consumers = new WeakMap();

function getConsumer(object) {
    if (!consumers.has(object)) {
        consumers.set(object, { stream: null, onAudioTrack: () => {} });
    }
    return consumers.get(object);
}

Object.defineProperties(HTMLAudioElement.prototype, {
    mediaStream: {
        get() {
            return getConsumer(this).stream;
        },
        set(stream) {
            let consumer = getConsumer(this);
            if (consumer.stream !== stream) {
                if (consumer.stream != null) {
                    consumer.stream.removeAudioConsumer(consumer);
                }
                if (stream == null) {
                    if (consumer.stream != null) {
                        this.srcObject = null;
                        consumer.onAudioTrack = function() {};
                        consumer.stream = null
                    }
                } else {
                    if (consumer.stream == null) {
                        this.srcObject = new MediaStream();
                        consumer.onAudioTrack = (audioTrack) => {
                            if (this.srcObject.getAudioTracks()[0] !== audioTrack) {
                                if (this.srcObject.getAudioTracks()[0]) {
                                    this.srcObject.removeTrack(this.srcObject.getAudioTracks()[0]);
                                }
                                if (audioTrack != null) {
                                    this.srcObject.addTrack(audioTrack);
                                }
                                this.srcObject = this.srcObject; // FIXME: Otherwise HTMLAudioElement wouldn't process the replacement of audio track properly...
                            }
                        };
                    }
                    stream.addAudioConsumer(consumer);
                }
                consumer.stream = stream;
            }
        }
    }
});