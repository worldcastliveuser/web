/**
 * @private
 */
export class Message {

    /**
     * @private
     */
    static fromDTO(conference, dto) {
        return new Message(conference, dto.id, dto.sentTo, dto.sentBy, dto.sentAt, dto.text, dto.persistent);
    }

    /**
     * @hideconstructor
     */
    constructor(conference, id, sentTo, sentBy, sentAt, text, persistent) {
        this.conference = conference;
        this.id = id;
        this.sentTo = sentTo;
        this.sentBy = sentBy;
        this.sentAt = sentAt;
        this.text = text;
        this.persistent = persistent;
    }

    getId() {
        return this.id;
    }

    getSentTo() {
        return this.sentTo;
    }

    getSentBy() {
        return this.sentBy;
    }

    getSentAt() {
        return this.sentAt;
    }

    getText() {
        return this.text;
    }

    isPersistent() {
        return this.persistent;
    }

    getParticipant() {
        return this.conference.getParticipantById(this.sentBy);
    }

}
