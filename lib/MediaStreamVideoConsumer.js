/**
 * @interface MediaStreamVideoConsumer
 *
 * @package
 */

/**
 * @method
 * @name MediaStreamVideoConsumer#onVideoTrack
 * @param {VideoTrack} videoTrack
 */