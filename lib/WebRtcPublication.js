import "webrtc-adapter";

/**
 * @implements {MediaStreamAudioConsumer}
 * @implements {MediaStreamVideoConsumer}
 *
 * @package
 */
export class WebRtcPublication {

    constructor(context, mediaURI) {
        this.publishURI = mediaURI + "/publish";
        this.unpublishURI = mediaURI + "/unpublish";
        this.context = context;
        this.pc = null;
        this.recoveryDelay = 0;
        this.recoveryTimeoutId = null;
        this.recoveryStartTimestamp = null;
        this.recoveryDuration = 0;
        this.httpPost = null;
        this.audioTransceiver = null;
        this.videoTransceiver = null;
        this.audioTrack = null;
        this.videoTrack = null;
        this.stream = null;
        this.destroyed = false;
    }

    getMediaStream() {
        return this.stream;
    }

    setMediaStream(stream) {
        if (this.destroyed) {
            throw new Error("WebRTC publication to `" + this.publishURI + "` has been already destroyed")
        }
        if (this.stream !== stream) {
            if (this.stream != null) {
                this.stream.removeAudioConsumer(this);
                this.stream.removeVideoConsumer(this);
            }
            if (stream != null){
                stream.addAudioConsumer(this);
                stream.addVideoConsumer(this);
            }
            this.stream = stream;
        }
    }

    hasAudioTrack() {
        return this.audioTrack != null;
    }

    hasVideoTrack() {
        return this.videoTrack != null;
    }

    destroy() {
        this.destroyed = true;
        this.release();
        this.audioTrack = null;
        this.videoTrack = null;
        if (this.stream != null) {
            this.stream.removeAudioConsumer(this);
            this.stream.removeVideoConsumer(this);
            this.stream = null;
        }
    }

    onAudioTrack(audioTrack) {
        if (!this.destroyed) {
            if (this.audioTrack !== audioTrack) {
                this.audioTrack = audioTrack;
                if (this.audioTrack != null || this.videoTrack != null) {
                    this.acquire();
                } else {
                    this.release();
                }
            }
        }
    }

    onVideoTrack(videoTrack) {
        if (!this.destroyed) {
            if (this.videoTrack !== videoTrack) {
                this.videoTrack = videoTrack;
                if (this.videoTrack != null || this.audioTrack != null) {
                    this.acquire();
                } else {
                    this.release();
                }
            }
        }
    }

    /**
     * @private
     */
    acquire() {
        let recover = (error) => {
            console.warn("WebRTC publication to `" + this.publishURI + "` will be recovered in " + this.recoveryDelay + " seconds after a failure:", error);
            this.abort();
            if (this.recoveryStartTimestamp == null) {
                this.recoveryStartTimestamp = Date.now();
            }
            this.recoveryTimeoutId = setTimeout(() => this.acquire(), this.recoveryDelay * 1000);
            this.recoveryDelay = Math.min(this.recoveryDelay + 1, 10);
        };
        try {
            if (this.pc == null || !this.pc.addTransceiver) {
                this.abort();
                this.report = {};
                this.pc = new RTCPeerConnection();
                this.pc.onicegatheringstatechange = (event) => {
                    if (this.pc.iceGatheringState === "complete" && this.pc.localDescription) { // It would be better to use `event.target.iceGatheringState ` instead, but it isn't available in Edge
                        let requestDTO = { type: "offer", sdp: this.pc.localDescription.sdp };
                        this.context.newHttpPost(this.publishURI, requestDTO, (cancel) => this.httpPost = { cancel: cancel }).then((responseDTO) => {
                            let description = new RTCSessionDescription({ type: "answer", sdp: responseDTO.sdp });
                            return this.pc.setRemoteDescription(description);
                        }).catch((error) => {
                            recover(error);
                        });
                    }
                };
                this.pc.onconnectionstatechange = (event) => {
                    switch (event.target.connectionState) {
                        case "connected":
                            this.recoveryDelay = 0;
                            if (this.recoveryStartTimestamp != null) {
                                this.recoveryDuration += Date.now() - this.recoveryStartTimestamp;
                                this.recoveryStartTimestamp = null;
                            }
                            break;
                        case "failed":
                            recover(new Error("Connection failed"));
                            break;
                    }
                };
                this.pc.onnegotiationneeded = (event) => {
                    this.pc.createOffer(this.pc.addTransceiver ? {} : { offerToReceiveAudio: false, offerToReceiveVideo: false }).then((description) => {
                        description.sdp = description.sdp.replace(/sendrecv/g, "sendonly"); // FIXME: This is a workaround for Safari 11
                        return this.pc.setLocalDescription(description);
                    }).then(() => {
                        if (this.pc.iceGatheringState === "complete") {
                            this.pc.onicegatheringstatechange();
                        }
                    }).catch((error) => {
                        recover(error);
                    });
                };
                if (this.pc.addTransceiver) {
                    this.audioTransceiver = this.pc.addTransceiver("audio", { direction: "inactive" });
                    this.videoTransceiver = this.pc.addTransceiver("video", { direction: "inactive" });
                }
            }
            if (this.pc.addTransceiver) {
                this.audioTransceiver.sender.replaceTrack(this.audioTrack);
                this.audioTransceiver.direction = this.audioTrack != null ? "sendonly" : "inactive";
                this.videoTransceiver.sender.replaceTrack(this.videoTrack);
                this.videoTransceiver.direction = this.videoTrack != null ? "sendonly" : "inactive";
            } else {
                if (this.audioTrack != null) {
                    this.pc.addTrack(this.audioTrack);
                }
                if (this.videoTrack) {
                    this.pc.addTrack(this.videoTrack);
                }
            }
        } catch (error) {
            recover(error);
        }
    }

    /**
     * @private
     */
    release() {
        this.abort();
        if (!this.destroyed) {
            this.context.newHttpPost(this.unpublishURI, {}, (cancel) => this.httpPost = {cancel: cancel});
        }
        this.recoveryDelay = 0;
        this.recoveryStartTimestamp = null;
        this.recoveryDuration = 0;
    }

    /**
     * @private
     */
    abort() {
        if (this.pc != null) {
            this.pc.close();
            this.pc = null;
            this.audioTransceiver = null;
            this.videoTransceiver = null;
        }
        if (this.httpPost != null) {
            this.httpPost.cancel();
            this.httpPost = null;
        }
        if (this.recoveryTimeoutId != null) {
            clearTimeout(this.recoveryTimeoutId);
            this.recoveryTimeoutId = null;
        }
    }

    getStats() {
        if (this.pc == null) {
            return Promise.resolve(null);
        }
        return new Promise((resolve, reject) => {
            this.pc.getStats().then((report) => {
                // for (let [key, value]  of report) { console.log(value.type, value); }
                if (this.recoveryStartTimestamp) {
                    this.recoveryDuration += Date.now() - this.recoveryStartTimestamp;
                    this.recoveryStartTimestamp = Date.now();
                }
                report = Array.from(report.values());
                let transport = report.filter((value) => value.type.match(/^transport$/))[0] || {};
                let candidatePair = report.filter((value) => value.type === "candidate-pair" && (transport.selectedCandidatePairId ? value.id === transport.selectedCandidatePairId : value.selected))[0] || {};
                let localCandidate = report.filter((value) => value.type === "local-candidate" && value.id === candidatePair.localCandidateId)[0] || {};
                let audioOutboundRtp = report.filter((value) => value.type === "outbound-rtp" && (value.kind === "audio" || value.mediaType === "audio"))[0] || {};
                let videoOutboundRtp = report.filter((value) => value.type === "outbound-rtp" && (value.kind === "video" || value.mediaType === "video"))[0] || {};
                let audioRemoteInboundRtp = report.filter((value) => value.type === "remote-inbound-rtp" && (value.kind === "audio" || value.mediaType === "audio"))[0] || {};
                let videoRemoteInboundRtp = report.filter((value) => value.type === "remote-inbound-rtp" && (value.kind === "video" || value.mediaType === "video"))[0] || {};
                let audioTrack = report.filter((value) => value.type === "track" && 'audioLevel' in value)[0] || {};
                let videoTrack = report.filter((value) => value.type === "track" && 'frameHeight' in value)[0] || {};
                let oldReport = this.report;
                let newReport = {
                    timestamp: Date.now(),
                    downtime: this.recoveryDuration,
                    bytesReceived: Number(candidatePair.bytesReceived),
                    bytesSent: Number(candidatePair.bytesSent),
                    audioPacketsSent: Number(audioOutboundRtp.packetsSent),
                    audioPacketsLost: Number(audioRemoteInboundRtp.packetsLost),
                    audioPacketsNotAcknowledgedReceived: Number(audioOutboundRtp.nackCount),
                    audioSamplesSent: Number(audioTrack.totalSamplesSent),
                    videoPacketsSent: Number(videoOutboundRtp.packetsSent),
                    videoPacketsLost: Number(videoRemoteInboundRtp.packetsLost),
                    videoPacketsNotAcknowledgedReceived: Number(videoOutboundRtp.nackCount),
                    videoFramesSent: Number(videoTrack.framesSent)
                };
                this.report = newReport;
                let stats = {
                    id: (this.publishURI.includes("secondary") ? "secondaryPublication" : "publication") + ":" + this.publishURI.replace(/\/media.*/, "").replace(/.*\//, ""),
                    protocol: localCandidate.protocol || "unknown",
                    downtime: Math.round((newReport.downtime - oldReport.downtime) / (newReport.timestamp - oldReport.timestamp) * 100), // %
                    rtt: Math.round((isNaN(candidatePair.currentRoundTripTime) ? Math.max(videoRemoteInboundRtp.roundTripTime, audioRemoteInboundRtp.roundTripTime) : Number(candidatePair.currentRoundTripTime)) * 1000), // ms
                    // jitter: Math.round(Math.max(Number(audioRemoteInboundRtp.jitter), Number(videoRemoteInboundRtp.jitter)) * 1000), // ms
                    bitrate: {
                        incoming: Math.round((newReport.bytesReceived - oldReport.bytesReceived) * 8 / (newReport.timestamp - oldReport.timestamp)), // kbit/s
                        outgoing: Math.round((newReport.bytesSent - oldReport.bytesSent) * 8 / (newReport.timestamp - oldReport.timestamp)) // kbit/s
                    },
                };
                if (newReport.audioPacketsSent > 0) {
                    stats.audio = {
                        sps: Math.round((newReport.audioSamplesSent - oldReport.audioSamplesSent) / (newReport.timestamp - oldReport.timestamp) * 1000),
                        losses: Math.round((newReport.audioPacketsNotAcknowledgedReceived - oldReport.audioPacketsNotAcknowledgedReceived) / (newReport.audioPacketsSent - oldReport.audioPacketsSent) * 100) // %
                    }
                }
                if (newReport.videoPacketsSent > 0) {
                    stats.video = {
                        fps: Math.round((newReport.videoFramesSent - oldReport.videoFramesSent) / (newReport.timestamp - oldReport.timestamp) * 1000),
                        width: Number(videoTrack.frameWidth),
                        height: Number(videoTrack.frameHeight),
                        losses: Math.round((newReport.videoPacketsNotAcknowledgedReceived - oldReport.videoPacketsNotAcknowledgedReceived) / (newReport.videoPacketsSent - oldReport.videoPacketsSent) * 100) // %
                    }
                }
                resolve(stats);
            });
        });
    }

}
