let consumers = new WeakMap();

function getConsumer(object) {
    if (!consumers.has(object)) {
        consumers.set(object, { stream: null, onAudioTrack: () => {}, onVideoTrack: () => {} });
    }
    return consumers.get(object);
}

Object.defineProperties(HTMLVideoElement.prototype, {
    mediaStream: {
        get() {
            return getConsumer(this).stream;
        },
        set(stream) {
            let consumer = getConsumer(this);
            if (consumer.stream !== stream) {
                if (consumer.stream != null) {
                    consumer.stream.removeAudioConsumer(consumer);
                    consumer.stream.removeVideoConsumer(consumer);
                }
                if (stream == null) {
                    if (consumer.stream != null) {
                        this.srcObject = null;
                        consumer.onAudioTrack = function() {};
                        consumer.onVideoTrack = function() {};
                        consumer.stream = null
                    }
                } else {
                    if (consumer.stream == null) {
                        this.srcObject = new MediaStream();
                        consumer.onAudioTrack = (audioTrack) => {
                            if (this.srcObject.getAudioTracks()[0] !== audioTrack) {
                                if (this.srcObject.getAudioTracks()[0]) {
                                    this.srcObject.removeTrack(this.srcObject.getAudioTracks()[0]);
                                }
                                if (audioTrack != null) {
                                    this.srcObject.addTrack(audioTrack);
                                }
                                this.srcObject = this.srcObject; // FIXME: Otherwise HTMLVideoElement wouldn't process the replacement of audio track properly...
                            }
                        };
                        consumer.onVideoTrack = (videoTrack) => {
                            if (this.srcObject.getVideoTracks()[0] !== videoTrack) {
                                if (this.srcObject.getVideoTracks()[0]) {
                                    this.srcObject.removeTrack(this.srcObject.getVideoTracks()[0]);
                                }
                                if (videoTrack != null) {
                                    this.srcObject.addTrack(videoTrack);
                                }
                                this.srcObject = this.srcObject; // FIXME: Otherwise HTMLVideoElement wouldn't process the replacement of video track properly...
                            }
                        };
                    }
                    stream.addAudioConsumer(consumer);
                    stream.addVideoConsumer(consumer);
                }
                consumer.stream = stream;
            }
        }
    }
});