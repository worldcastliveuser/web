/**
 * @deprecated Is not in use any more.
 */
const StreamingMode = {

    /**
     * @deprecated Is not in use any more.
     */
    VIDEO_AND_AUDIO: { video: true, audio: true },

    /**
     * @deprecated Is not in use any more.
     */
    VIDEO_ONLY: { video: true, audio: false },

    /**
     * @deprecated Is not in use any more.
     */
    AUDIO_ONLY: { video: false, audio: true },

};

export { StreamingMode };