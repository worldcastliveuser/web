import {ConferenceLayout} from "./ConferenceLayout";
import {MediaStream} from "./MediaStream";
import {ParticipantRole} from "./ParticipantRole";
import {ParticipantMedia} from "./ParticipantMedia";
import {WebRtcSubscription} from "./WebRtcSubscription";
import isEqual from "lodash/isEqual";

/**
 * Participant class is used for representing remote participants (each instance represents a single remote
 * participants). The participant on behalf of whom web-application is participating in the conference (aka the local
 * participant) is represented with {@link Me} class. You can get representations of all remote participants and a
 * representation of a specific remote participant with {@link Conference#getParticipants getParticipants} and
 * {@link Conference#getParticipantById getParticipantById} methods of {@link Conference} class, respectively.
 * Participant class contains methods for getting and setting parameters of the remote participant and for getting its
 * primary and secondary media streams:
 *
 * ```
 * let participantVideo = document.getElementById("participantVideo");
 * let participantSecondaryVideo = document.getElementById("mySecondaryVideo");
 *
 * let participant = conference.getParticipantById("<PARTICIPANT_ID>");
 *
 * let participantStream = participant.getMediaStream();
 * participantVideo.mediaStream = participantStream;
 *
 * let participantSecondaryStream = participant.getSecondaryMediaStream();
 * participantSecondaryVideo.mediaStream = participantSecondaryStream;
 * ```
 */
export class Participant {

    /**
     * @private
     */
    static fromDTO(context, dto) {
        return new Participant(context, dto.id, dto.name, dto.role, dto.layout, ParticipantMedia.fromDTO(dto.media), ParticipantMedia.fromDTO(dto.secondaryMedia));
    }

    /**
     * @hideconstructor
     */
    constructor(context, id, name, role, layout, media, secondaryMedia) {
        this.context = context;
        this.id = id;
        this.name = name;
        this.role = role;
        this.layout = layout;
        this.media = media;
        this.subscription = new WebRtcSubscription(context, "/participants/" + id + "/media");
        this.subscription.setConstraints(media.isAudio(), media.isVideo());
        this.mediaStream = new MediaStream(this.subscription, this.subscription);
        this.rawMediaStream = WebRtcSubscription.createRawMediaStream(this.mediaStream);
        this.secondaryMedia = secondaryMedia;
        this.secondarySubscription = new WebRtcSubscription(context, "/participants/" + id + "/secondaryMedia");
        this.secondarySubscription.setConstraints(secondaryMedia.isAudio(), secondaryMedia.isVideo());
        this.secondaryMediaStream = new MediaStream(this.secondarySubscription, this.secondarySubscription);
        this.secondaryRawMediaStream = WebRtcSubscription.createRawMediaStream(this.secondaryMediaStream);
    }

    /**
     * Return the ID of the remote participant. The ID is unique and never changes.
     *
     * @returns {String} The ID of the remote participant.
     */
    getId() {
        return this.id;
    }

    /**
     * Returns the current name of the remote participant. The name of the remote participant can be shown above his
     * video in the conference media stream and recording.
     *
     * @returns {String} The current name of the remote participant.
     */
    getName() {
        return this.name;
    }

    /**
     * Changes the name of the remote participant. The name of the remote participant can be shown above his video in
     * the conference media stream and recording. The name changing is an asynchronous operation, that's why this
     * method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an
     * `Error` (if the operation fails). The operation can succeed only if the {@link Me local participant} plays a
     * role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {String} name The new name for the remote participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setName(name) {
        let requestDTO = { name: name };
        return this.context.newHttpPatch("/participants/" + this.id, requestDTO).then((responseDTO) => {
            this.name = responseDTO.name;
        });
    }

    /**
     * Returns the current {@link ParticipantRole role} of the remote participant. The role defines a set of
     * permissions which the remote participant is granted.
     *
     * @returns {ParticipantRole} The current role of the remote participant.
     */
    getRole() {
        return this.role;
    }

    /**
     * Changes the {@link ParticipantRole role} of the remote participant. The role defines a set of permissions which
     * the remote participant is granted. The role changing is an asynchronous operation, that's why this method
     * returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error
     * (if the operation fails). The operation can succeed only if the {@link Me local participant} plays a role of a
     * {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {ParticipantRole} role The new role for the remote participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setRole(role) {
        let requestDTO = { role: role };
        return this.context.newHttpPatch("/participants/" + this.id, requestDTO).then((responseDTO) => {
            this.role = responseDTO.role;
        });
    }

    /**
     * Returns the current {@link ConferenceLayout layout} of the remote participant. The layout determines arrangement
     * of videos in {@link Conference#getMediaStream conference media stream} which the participant receives.
     *
     * @returns {ConferenceLayout} The current layout of the remote participant.
     */
    getLayout() {
        return this.layout;
    }

    /**
     * Changes the {@link ConferenceLayout layout} of the remote participant. The layout determines arrangement of
     * videos in {@link Conference#getMediaStream conference media stream} which the participant receives. The layout
     * changing is an asynchronous operation, that's why this method returns a `Promise` that either resolves with no
     * value (if the operation succeeds) or rejects with an `Error` (if the operation fails). The operation can
     * succeed only if the {@link Me local participant} plays a role of a {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {ConferenceLayout} layout The new layout for the remote participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setLayout(layout) {
        let requestDTO = { layout: layout };
        return this.context.newHttpPatch("/participants/" + this.id, requestDTO).then((responseDTO) => {
            this.layout = responseDTO.layout;
        });
    }

    /**
     * Checks whether the remote participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and {@link Participant#isStreamingVideo isStreamingVideo} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the remote participant is streaming primary audio or not.
     */
    isStreamingAudio() {
        return this.media.isAudio();
    }

    /**
     * Checks whether the remote participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and {@link Participant#isStreamingAudio isStreamingAudio} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the remote participant is streaming primary video or not.
     */
    isStreamingVideo() {
        return this.media.isVideo();
    }

    /**
     * Returns the {@link MediaStream primary media stream} of the remote participant. The primary media stream is
     * intended for streaming video and audio taken from a camera and a microphone of the participant's computer,
     * respectively. You can get and play the primary media stream at any moment regardless of whether the participant
     * is streaming its primary video/audio or not: if the participant started or stopped streaming its primary video
     * or/and audio, the returned media stream would be updated automatically.
     *
     * @returns {MediaStream} The primary media stream of the remote participant.
     */
    getMediaStream() {
        if (arguments.length > 0) {
            console.warn("MindSDK: Calling Participant.getMediaStream(mode) is deprecated, use Participant.getMediaStream() instead");
            if (arguments[0].audio) {
                this.mediaStream.addAudioConsumer(this.rawMediaStream);
            } else {
                this.mediaStream.removeAudioConsumer(this.rawMediaStream);
            }
            if (arguments[0].video) {
                this.mediaStream.addVideoConsumer(this.rawMediaStream);
            } else {
                this.mediaStream.removeVideoConsumer(this.rawMediaStream);
            }
            return this.rawMediaStream;
        } else {
            return this.mediaStream;
        }
    }

    /**
     * Checks whether the remote participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and {@link Participant#isStreamingSecondaryVideo isStreamingSecondaryVideo} return `false` then
     * the participant is not streaming secondary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the remote participant is streaming secondary audio or not.
     */
    isStreamingSecondaryAudio() {
        return this.secondaryMedia.isAudio();
    }

    /**
     * Checks whether the remote participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and {@link Participant#isStreamingSecondaryAudio isStreamingSecondaryAudio} return `false` then
     * the participant is not streaming secondary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the remote participant is streaming secondary video or not.
     */
    isStreamingSecondaryVideo() {
        return this.secondaryMedia.isVideo();
    }

    /**
     * Returns the {@link MediaStream secondary media stream} of the remote participant. The secondary media stream is
     * intended for streaming an arbitrary audio/video content, e.g. for sharing a screen of the participant's computer.
     * You can get and play the secondary media stream at any moment regardless of whether the participant is streaming
     * its secondary video/audio or not: if the participant started or stopped streaming its secondary video or/and
     * audio, the returned media stream would be updated automatically.
     *
     * @returns {MediaStream} The secondary media stream of the remote participant.
     */
    getSecondaryMediaStream() {
        if (arguments.length > 0) {
            console.warn("MindSDK: Calling Participant.getSecondaryMediaStream(mode) is deprecated, use Participant.getSecondaryMediaStream() instead");
            if (arguments[0].audio) {
                this.secondaryMediaStream.addAudioConsumer(this.secondaryRawMediaStream);
            } else {
                this.secondaryMediaStream.removeAudioConsumer(this.secondaryRawMediaStream);
            }
            if (arguments[0].video) {
                this.secondaryMediaStream.addVideoConsumer(this.secondaryRawMediaStream);
            } else {
                this.secondaryMediaStream.removeVideoConsumer(this.secondaryRawMediaStream);
            }
            return this.secondaryRawMediaStream;
        } else {
            return this.secondaryMediaStream;
        }
    }

    /**
     * @private
     */
    update(dto, fire) {
        if (this.name !== dto.name) {
            this.name = dto.name;
            if (fire) {
                this.context.fireOnParticipantNameChanged(this);
            }
        }
        if (this.role !== dto.role) {
            this.role = dto.role;
            if (fire) {
                this.context.fireOnParticipantRoleChanged(this);
            }
        }
        if (this.layout !== dto.layout) {
            this.layout = dto.layout;
            if (fire) {
                this.context.fireOnParticipantLayoutChanged(this);
            }
        }
        let media = ParticipantMedia.fromDTO(dto.media);
        if (!isEqual(this.media, media)) {
            this.media = media;
            this.subscription.setConstraints(media.isAudio(), media.isVideo());
            if (fire) {
                this.context.fireOnParticipantMediaChanged(this);
            }
        }
        let secondaryMedia = ParticipantMedia.fromDTO(dto.secondaryMedia);
        if (!isEqual(this.secondaryMedia, secondaryMedia)) {
            this.secondaryMedia = secondaryMedia;
            this.secondarySubscription.setConstraints(secondaryMedia.isAudio(), secondaryMedia.isVideo());
            if (fire) {
                this.context.fireOnParticipantSecondaryMediaChanged(this);
            }
        }
    }

    /**
     * @private
     */
    destroy() {
        this.subscription.destroy();
        this.secondarySubscription.destroy();
    }

}
