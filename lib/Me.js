import {MediaStream} from "./MediaStream";
import {Participant} from "./Participant";
import {ParticipantMedia} from "./ParticipantMedia";
import {ParticipantRole} from "./ParticipantRole";
import {WebRtcPublication} from "./WebRtcPublication";

/**
 * Me class is used for representing participant on behalf of whom web-application is participating in the conference
 * (aka the local participant). All other participants are considered to be remote and represented with
 * {@link Participant} class. You can get a representation of the local participant with {@link Conference#getMe getMe}
 * method of {@link Conference} class. Me is a subclass of {@link Participant} class, so that it inherits all public
 * methods of the superclass and adds methods for setting primary and secondary media streams that should be sent on
 * behalf of the local participant, and for sending messages from the local participant to other participant(s) or to
 * the server part of your application:
 *
 * ```
 * let me = conference.getMe();
 * let microphone = MindSDK.getMicrophones()[0];
 * let camera = MindSDK.getCameras()[0];
 * let myStream = MindSDK.createMediaStream(microphone, camera);
 * me.setMediaStream(myStream);
 * Promise.all([ microphone.acquire(), camera.acquire() ]).catch(function(error) {
 *     alert("Can't acquire camera or microphone: " + error);
 * });
 *
 * ....
 *
 * let screen = MindSDK.getScreen();
 * if (screen) {
 *     let mySecondaryStream = MindSDK.createMediaStream(null, screen);
 *     me.setSecondaryMediaStream(mySecondaryStream);
 *     screen.acquire().catch(function(error) {
 *         alert("Can't acquire screen: " + error);
 *     });
 * }
 *
 * ...
 *
 * me.sendMessageToAll("Hello, everybody!");
 * me.sendMessageToApplication("Hello, the server part of the application!");
 * let participant = conference.getParticipantById("<PARTICIPANT_ID>");
 * if (participant) {
 *     me.sendMessageToParticipant("Hello, " + participant.getName(), participant);
 * }
 * ```
 */
export class Me extends Participant {

    /**
     * @private
     */
    static fromDTO(context, dto) {
        return new Me(context, dto.id, dto.name, dto.role, dto.layout, new ParticipantMedia(false, false), new ParticipantMedia(false, false));
    }

    /**
     * @hideconstructor
     */
    constructor(context, id, name, role, layout, media, secondaryMedia) {
        super(context, id, name, role, layout, media, secondaryMedia);
        this.publication = new WebRtcPublication(context, "/participants/" + id + "/media");
        this.secondaryPublication = new WebRtcPublication(context, "/participants/" + id + "/secondaryMedia");
    }

    /**
     * Return the ID of the local participant. The ID is unique and never changes.
     *
     * @returns {String} The ID of the local participant.
     */
    getId() {
        return super.getId();
    }

    /**
     * Returns the current name of the local participant. The name of the local participant can be shown above his
     * video in the conference media stream and recording.
     *
     * @returns {String} The current name of the local participant.
     */
    getName() {
        return super.getName();
    }

    /**
     * Changes the name of the local participant. The name of the local participant can be shown above his video in the
     * conference media stream and recording. The name changing is an asynchronous operation, that's why this method
     * returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error`
     * (if the operation fails).
     *
     * @param {String} name The new name for the local participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setName(name) {
        return super.setName(name);
    }

    /**
     * Returns the current {@link ParticipantRole role} of the local participant. The role defines a set of permissions
     * which the local participant is granted.
     *
     * @returns {ParticipantRole} The current role of the local participant.
     */
    getRole() {
        return super.getRole();
    }

    /**
     * Changes the {@link ParticipantRole role} of the local participant. The role defines a set of permissions which
     * the local participant is granted. The role changing is an asynchronous operation, that's why this method
     * returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error (if
     * the operation fails). The operation can succeed only if the {@link Me local participant} plays a role of a
     * {@link ParticipantRole.MODERATOR moderator}.
     *
     * @param {ParticipantRole} role The new role for the local participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setRole(role) {
        return super.setRole(role);
    }

    /**
     * Returns the current {@link ConferenceLayout layout} of the local participant. The layout determines arrangement
     * of videos in {@link Conference#getMediaStream conference media stream} which the participant receives.
     *
     * @returns {ConferenceLayout} The current layout of the local participant.
     */
    getLayout() {
        return super.getLayout();
    }

    /**
     * Changes the {@link ConferenceLayout layout} of the local participant. The layout determines arrangement of
     * videos in {@link Conference#getMediaStream conference media stream} which the participant receives. The layout
     * changing is an asynchronous operation, that's why this method returns a `Promise` that either resolves with no
     * value (if the operation succeeds) or rejects with an `Error` (if the operation fails).
     *
     * @param {ConferenceLayout} layout The new layout for the local participant.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    setLayout(layout) {
        return super.setLayout(layout);
    }

    /**
     * Checks whether the local participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and {@link Me#isStreamingVideo isStreamingVideo} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the local participant is streaming primary audio or not.
     */
    isStreamingAudio() {
        return this.publication.hasAudioTrack();
    }

    /**
     * Checks whether the local participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and {@link Me#isStreamingAudio isStreamingAudio} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the local participant is streaming primary video or not.
     */
    isStreamingVideo() {
        return this.publication.hasVideoTrack();
    }

    /**
     * Returns {@link MediaStream media stream} which is being streamed on behalf of the local participant as the
     * primary media stream or `null` value if the local participant is not streaming the primary media stream at the
     * moment. The primary media stream is intended for streaming video and audio taken from a camera and a microphone
     * of the computer, respectively.
     *
     * @returns {MediaStream|null} The current primary media stream of the local participant.
     */
    getMediaStream() {
        if (arguments.length > 0) {
            console.warn("MindSDK: Calling Me.getMediaStream(mode) is deprecated, use Me.getMediaStream() instead");
        }
        if (this.rawMediaStream) {
            return this.rawMediaStream;
        } else {
            return this.publication.getMediaStream();
        }
    }

    /**
     * Sets {@link MediaStream media stream} for streaming on behalf of the local participant as the primary media
     * stream. The primary media stream is intended for streaming video and audio taken from a camera and a microphone
     * of the computer, respectively. If the primary media stream is already being streamed, then it will be replaced
     * with the passed one. Set `null` value to stop streaming the primary media stream on behalf of the local
     * participant.
     *
     * @param {MediaStream|null} stream The new primary media stream of the local participant.
     */
    setMediaStream(stream) {
        if (stream instanceof window.MediaStream) {
            console.warn("MindSDK: Passing the raw `MediaStream` to Me.setMediaStream() is deprecated, instead pass `MediaStream` created with MindSDK.createMediaStream(microphone, camera)");
            let audioTrack = stream.getAudioTracks().length > 0 ? stream.getAudioTracks()[0] : null;
            let videoTrack = stream.getVideoTracks().length > 0 ? stream.getVideoTracks()[0] : null;
            let mediaStream = this.publication.getMediaStream();
            if (mediaStream == null) {
                mediaStream = new MediaStream();
                mediaStream.addAudioConsumer = () => {
                    this.publication.onAudioTrack(audioTrack);
                }
                mediaStream.removeAudioConsumer = () => {
                    this.publication.onAudioTrack(null);
                }
                mediaStream.addVideoConsumer = () => {
                    this.publication.onVideoTrack(videoTrack);
                }
                mediaStream.removeVideoConsumer = () => {
                    this.publication.onVideoTrack(null);
                }
            } else {
                this.publication.onAudioTrack(audioTrack);
                this.publication.onVideoTrack(videoTrack);
            }
            this.rawMediaStream = stream;
            this.publication.setMediaStream(mediaStream);
        } else {
            this.rawMediaStream = null;
            this.publication.setMediaStream(stream);
        }
    }

    /**
     * Checks whether the local participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and {@link Me#isStreamingSecondaryVideo isStreamingSecondaryVideo} return `false` then the
     * participant is not streaming secondary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the local participant is streaming secondary audio or not.
     */
    isStreamingSecondaryAudio() {
        return this.secondaryPublication.hasAudioTrack();
    }

    /**
     * Checks whether the local participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and {@link Me#isStreamingSecondaryAudio isStreamingSecondaryAudio} return `false` then the
     * participant is not streaming secondary media stream at all.
     *
     * @returns {Boolean} The boolean value which indicates if the local participant is streaming secondary video or not.
     */
    isStreamingSecondaryVideo() {
        return this.secondaryPublication.hasVideoTrack();
    }

    /**
     * Returns {@link MediaStream media stream} which is being streamed on behalf of the local participant as the
     * secondary media stream or `null` value if the local participant is not streaming the secondary media stream at
     * the moment. The secondary media stream is intended for streaming an arbitrary audio/video content, e.g. for
     * sharing a screen of the computer.
     *
     * @returns {MediaStream|null} The current secondary media stream of the local participant.
     */
    getSecondaryMediaStream() {
        if (arguments.length > 0) {
            console.warn("MindSDK: Calling Me.getSecondaryMediaStream(mode) is deprecated, use Me.getSecondaryMediaStream() instead");
        }
        if (this.rawSecondaryMediaStream) {
            return this.rawSecondaryMediaStream;
        } else {
            return this.secondaryPublication.getMediaStream();
        }
    }

    /**
     * Sets {@link MediaStream media stream} for streaming on behalf of the local participant as the secondary media
     * stream. The secondary media stream is intended for streaming an arbitrary audio/video content, e.g. for sharing
     * a screen of the computer. If the secondary media stream is already being streamed, then it will be replaced with
     * the passed one. Set `null` value to stop streaming the secondary media stream on behalf of the local participant.
     *
     * @param {MediaStream|null} stream The new secondary media stream of the local participant.
     */
    setSecondaryMediaStream(stream) {
        if (stream instanceof window.MediaStream) {
            console.warn("MindSDK: Passing the raw `MediaStream` to Me.setSecondaryMediaStream() is deprecated, instead pass `MediaStream` created with MindSDK.createMediaStream(screen)");
            let audioTrack = stream.getAudioTracks().length > 0 ? stream.getAudioTracks()[0] : null;
            let videoTrack = stream.getVideoTracks().length > 0 ? stream.getVideoTracks()[0] : null;
            let mediaStream = this.secondaryPublication.getMediaStream();
            if (mediaStream == null) {
                mediaStream = new MediaStream();
                mediaStream.addAudioConsumer = () => {
                    this.secondaryPublication.onAudioTrack(audioTrack);
                }
                mediaStream.removeAudioConsumer = () => {
                    this.secondaryPublication.onAudioTrack(null);
                }
                mediaStream.addVideoConsumer = () => {
                    this.secondaryPublication.onVideoTrack(videoTrack);
                }
                mediaStream.removeVideoConsumer = () => {
                    this.secondaryPublication.onVideoTrack(null);
                }
            } else {
                this.secondaryPublication.onAudioTrack(audioTrack);
                this.secondaryPublication.onVideoTrack(videoTrack);
            }
            this.rawSecondaryMediaStream = stream;
            this.secondaryPublication.setMediaStream(mediaStream);
        } else {
            this.rawSecondaryMediaStream = null;
            this.secondaryPublication.setMediaStream(stream);
        }
    }

    /**
     * Sends a text message on behalf of the local participant to the server part of your application. The message
     * sending is an asynchronous operation, that's why this method returns a `Promise` that either resolves with no
     * value (if the operation succeeds) or rejects with an `Error (if the operation fails).
     *
     * @param {String} message The text of the message.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    sendMessageToApplication(message) {
        let requestDTO = { sendTo: this.context.getApplicationId(), text: message, persistent: false };
        return this.context.newHttpPost("/messages", requestDTO);
    }

    /**
     * Sends a text message on behalf of the local participant to the specified participant. The message sending is an
     * asynchronous operation, that's why this method returns a `Promise` that either resolves with no value (if the
     * operation succeeds) or rejects with an `Error (if the operation fails).
     *
     * @param {String} message The text of the message.
     * @param {Participant} participant The participant which the message should be sent to.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    sendMessageToParticipant(message, participant) {
        let requestDTO = { sendTo: participant.getId(), text: message, persistent: false };
        return this.context.newHttpPost("/messages", requestDTO);
    }

    /**
     * Sends a text message on behalf of the local participant to all in the conference, i.e. to the server part of
     * your application and to all participants at once. The message sending is an asynchronous operation, that's why
     * this method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with
     * an `Error (if the operation fails).
     *
     * @param {String} message The text of the message.
     *
     * @returns {Promise} The promise that either resolves with no value or rejects with an `Error`.
     */
    sendMessageToAll(message) {
        let requestDTO = { sendTo: this.context.getConferenceId(), text: message, persistent: false };
        return this.context.newHttpPost("/messages", requestDTO);
    }

    /**
     * @private
     */
    update(dto, fire) {
        if (this.name !== dto.name) {
            this.name = dto.name;
            if (fire) {
                this.context.fireOnMeNameChanged(this);
            }
        }
        if (this.role !== dto.role) {
            this.role = dto.role;
            if (fire) {
                this.context.fireOnMeRoleChanged(this);
            }
        }
        if (this.layout !== dto.layout) {
            this.layout = dto.layout;
            if (fire) {
                this.context.fireOnMeLayoutChanged(this);
            }
        }
    }

    /**
     * @private
     */
    destroy() {
        this.publication.destroy();
        this.secondaryPublication.destroy();
        super.destroy();
    }

}
