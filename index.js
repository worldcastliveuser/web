import {CameraListener} from "./lib/CameraListener";
import {Conference} from "./lib/Conference";
import {ConferenceLayout} from "./lib/ConferenceLayout";
import {ConferenceListener} from "./lib/ConferenceListener";
import {MicrophoneListener} from "./lib/MicrophoneListener";
import {MindSDK} from "./lib/MindSDK";
import {ParticipantRole} from "./lib/ParticipantRole";
import {ScreenListener} from "./lib/ScreenListener";
import {StreamingMode} from "./lib/StreamingMode";

import "./lib/Audio";
import "./lib/Video";

export default {
    initialize: MindSDK.initialize,
    getMicrophones: MindSDK.getMicrophones,
    getCameras: MindSDK.getCameras,
    getScreen: MindSDK.getScreen,
    createMediaStream: MindSDK.createMediaStream,
    join: MindSDK.join,
    exit: MindSDK.exit,
    CameraListener: CameraListener,
    Conference: Conference,
    ConferenceLayout: ConferenceLayout,
    ConferenceListener: ConferenceListener,
    MicrophoneListener: MicrophoneListener,
    ParticipantRole: ParticipantRole,
    ScreenListener: ScreenListener,
    StreamingMode: StreamingMode
}
